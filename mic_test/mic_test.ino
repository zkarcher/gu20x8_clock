#include <stdbool.h>
#include <stdint.h>
#include "Const.h"

// VFD control
#define CD         (5)
#define WR         (18)	// falling edge latches Address & Data
//#define BUSY     (XX)	// INPUT: 1==busy with last operation
#define RESET      (15)	// Active-low, 0==resets device
#define EN         (23)	// 1==enables display, 0==blank screen

const uint8_t ADDRESS_PINS[] = {19, 4, 13, 32, 14, 33};

// Data: Shift register interface
#define DATA_OUT   (25)
#define DATA_CLOCK (27)
#define DATA_CLEAR (26)	// Active-low, so keep this high

// Mic pins
// OOooops ... GPIO16 doesn't support analogRead. See:
// https://randomnerdtutorials.com/esp32-pinout-reference-gpios/
#define MICOUT     (35)
#define MICGAIN    (17)

// Shift register pin order: 6,4,2,0, 7,5,3,1
const uint8_t DATA_MASKS[] = {
	0x40, 0x10, 0x04, 0x01,
	0x80, 0x20, 0x08, 0x02
};

typedef enum {
	k_normal = 0,
	k_reversed = 1
} BitOrder;

uint8_t blue[WIDTH];
uint8_t red[WIDTH];

void addressOut(uint8_t value)
{
	for (uint8_t i = 0; i < 6; i++) {
		uint8_t mask = 0x20 >> i;
		digitalWrite(ADDRESS_PINS[i], (value & mask) ? HIGH : LOW);
	}
}

void dataByteOut(uint8_t value, BitOrder bitOrder)
{
	for (int8_t i = 7; i >= 0; i--) {
		digitalWrite(DATA_CLOCK, LOW);

		uint8_t maskIdx = (bitOrder == k_reversed) ? (7 - i) : i;
		uint8_t mask = DATA_MASKS[maskIdx];
		digitalWrite(DATA_OUT, (value & mask) ? HIGH : LOW);

		// Clock 'em
		digitalWrite(DATA_CLOCK, HIGH);
	}
}

void setup() {
	Serial.begin(115200);
	//while (!Serial) {}
	Serial.println("Hello world!");

	pinMode(CD, OUTPUT);
	pinMode(WR, OUTPUT);
	//pinMode(BUSY, INPUT);
	pinMode(RESET, OUTPUT);
	pinMode(EN, OUTPUT);

	// Address pins
	for (uint8_t p = 0; p < 6; p++) {
		pinMode(ADDRESS_PINS[p], OUTPUT);
	}

	// Data pins
	pinMode(DATA_OUT, OUTPUT);
	pinMode(DATA_CLOCK, OUTPUT);
	pinMode(DATA_CLEAR, OUTPUT);

	// Mic pins
	pinMode(MICOUT, INPUT);
	pinMode(MICGAIN, OUTPUT);

	// HIGH == 40db, LOW == 50db, floating == 60db
	digitalWrite(MICGAIN, HIGH);

	// Start!
	digitalWrite(DATA_CLOCK, LOW);
	digitalWrite(DATA_CLEAR, HIGH);
	digitalWrite(CD, HIGH);
	digitalWrite(WR, HIGH);
	digitalWrite(RESET, HIGH);
	digitalWrite(EN, HIGH);

	// Dim plz
	if (SET_BRIGHTNESS) {

		addressOut(0x3f);
		dataByteOut(BRIGHTNESS, k_normal);

		// Unlatch
		digitalWrite(WR, HIGH);
		digitalWrite(CD, LOW);

		// Wait until not busy, then latch.
		//while (digitalRead(BUSY) == HIGH) {}
		delay(5);

		// OK to latch now.
		digitalWrite(WR, LOW);
		delay(5);

	}

	// Ready for data
	digitalWrite(CD, HIGH);
	delay(5);

	//
	// -- Clear the screen --
	//
	for (uint8_t i = 0; i < WIDTH; i++) {
		digitalWrite(WR, HIGH);	// unlatch

		const uint8_t RED_MASK = 0x20;
		const uint8_t BLUE_MASK = 0x0;
		addressOut(i | BLUE_MASK);

		dataByteOut(0x0, k_reversed);

		// OK to latch now.
		digitalWrite(WR, LOW);
		delay(1);
	}

}

void loop()
{
	// Record some audio, display on the screen
	for (uint8_t i = 0; i < WIDTH; i++) {

		digitalWrite(WR, HIGH);	// unlatch

		int audio = analogRead(MICOUT);

		/*
		Serial.println(audio);
		Serial.flush();
		*/

		uint8_t columnPixels = (0x1 << (audio / 400));

		const uint8_t RED_MASK = 0x20;
		addressOut(i | RED_MASK);

		dataByteOut(columnPixels, k_reversed);

		// Latch now
		digitalWrite(WR, LOW);
		delay(1);
	}

	delay(1000 / 250);
}
