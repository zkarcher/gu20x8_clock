#pragma once

#include <stdint.h>

//#define PI            (3.1415926f)

#define MAX(a,b)      ((a) > (b) ? (a) : (b))
#define MIN(a,b)      ((a) < (b) ? (a) : (b))

#define lerp(a,b,p)   ((a) + ((b) - (a)) * (p))

#define randBi(n)     (random(n) - (n) / 2)

inline float randf() {
	return random(INT32_MAX) * (1.0f / INT32_MAX);
}

inline float randf(float n) {
	return randf() * n;
}

inline float randf(float m, float n) {
	return m + randf() * (n - m);
}
