#pragma once

#include <stdbool.h>

#define WIDTH          (20)
#define HEIGHT         (8)

// Brightness range: [0..3]
// 0-2 are dimmer, also it WHINES aaaaaagh
// 3 is brighter, no perceptible whine.
#define SET_BRIGHTNESS (true)
#define BRIGHTNESS     (3)

// Convenient color flags
#define BLACK          (0)
#define BLUE           (1)
#define RED            (2)
#define WHITE          (BLUE | RED)

#define LAYER_COUNT    (5)
