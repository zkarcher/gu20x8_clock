#include <stdint.h>

#define DATA0      (25)
#define DATA1      (26)
#define CLOCK      (27)
#define CLEAR      (23)

const uint16_t COLORS[] = {
	0b1111111111111111,
	0b1010101010101010,
	0b1010100010101000,
	0b1010100010001000,
	0b1000100010001000,
	0b1000100010000000,
	0b1000000010000000,
	0b1000000000000000,

	0x0,
	0x0,
	0x0,
	0x0,
	0x0,
	0x0,
	0x0,
	0x0
};

uint16_t colors[16];

uint16_t step = 0xffff;
uint8_t walk = 0;

void setup() {
	pinMode(DATA0, OUTPUT);
	pinMode(DATA1, OUTPUT);
	pinMode(CLOCK, OUTPUT);
	pinMode(CLEAR, OUTPUT);
}

void loop() {
	if (step >= 500) {
		step = 0;

		// Redraw the pattern
		walk++;

		for (uint8_t i = 0; i < 16; i++) {
			colors[i] = COLORS[(walk + i) & 0b1111];
		}
	}

	step++;

	uint16_t mask = (uint16_t)(0x1) << (step & 0b1111);

	digitalWrite(CLEAR, LOW);
	digitalWrite(CLEAR, HIGH);

	// Push out all data
	for (int8_t i = 7; i >= 0; i--) {
		digitalWrite(CLOCK, LOW);
		digitalWrite(DATA0, (colors[i] & mask) ? HIGH : LOW);
		digitalWrite(DATA1, (colors[i + 8] & mask) ? HIGH : LOW);
		digitalWrite(CLOCK, HIGH);
	}

	delayMicroseconds(200);
}
