~~~~~~~~~~~~~~~~~~
~~    DONE!
~~~~~~~~~~~~~~~~~~

[X] Get time from WiFi

[X] Change every 60 seconds

[X] Serial print time every second

[X] Choose good color palettes only

[X] Animate different directions

[?] Time zones (Might be working? Haven't tested.)

[X] Fix clock, should only connect once, and not choke if the first connect fails

[X] 12hr vs 24hr

[X] Web server

[X] Web GUI: Support brightness levels, maybe more?

[X] long colorInterval: Change minutes




///////////////////
//   NEXT STEPS
///////////////////

++ On branch:  master



[X] WiFi: Connecting animation

[X] Multiple WiFi's?

- 192.168.2.11
	- Print the IP address on reset ?

- Schedule? Turn on at 9, off at 6 ?

- Auto-reboot at 3am?



- Mic test
	- Sound activated?




- Grayscale timing test
	- Scan rate == 100hz ?  delay(10) is pretty close.





EAGLE issues
------------

* GPIO 16 is not useable for mic input. Moved to GPIO 35. (Added jumper wire under the board.) Fix the EAGLE routing?






2019-09-27
----------

EAGLE board
Shift reg data: 25, 26, 27
WR:
BUSY:
EN:



2019-09-21
----------
Hmm! New pin map is not working.

          WORKED:   NOT WORKING:
CD        25        13
WR        26        12
BUSY      27        14
EN        14        27



Old (working):
-#define CD     (25)
-#define WR     (26)	// falling edge latches Address & Data
-#define BUSY   (27)	// INPUT: 1==busy with last operation
-#define EN     (14)	// 1==enables display, 0==blank screen
-const uint8_t ADDRESS_PINS[] = {1, 3, 22, 21, 19, 23};
-const uint8_t DATA_PINS[] = {18, 5, 17, 16, 4, 15, 12, 13};


New (not working):
+#define CD     (13)
+#define WR     (12)	// falling edge latches Address & Data
+#define BUSY   (14)	// INPUT: 1==busy with last operation
#define EN     (27)	// 1==enables display, 0==blank screen
const uint8_t ADDRESS_PINS[] = {26, 25, 15, 4, 16, 17};
+const uint8_t DATA_PINS[] = {5, 18, 23, 19, 21, 22, 3, 1};


GPIO14 == MTMS, HSPICLK
GPIO12 == MTDI, HSPIQ



Do not use JTAG pins for something else

Operation of JTAG may be disturbed, if some other h/w is connected to JTAG pins besides ESP32 module and JTAG adapter. ESP32 JTAG us using the following pins:

      	ESP32 JTAG Pin 	JTAG Signal
    1 	MTDO / GPIO15 	TDO
    2 	MTDI / GPIO12 	TDI
    3 	MTCK / GPIO13 	TCK
    4 	MTMS / GPIO14 	TMS



https://randomnerdtutorials.com/esp32-pinout-reference-gpios/

Pins HIGH at Boot

Some GPIOs change its state to HIGH or output PWM signals at boot or reset. This means that if you have outputs connected to these GPIOs you may get unexpected results when the ESP32 resets or boots.

    GPIO 1
    GPIO 3
    GPIO 5
    GPIO 6 to GPIO 11 (connected to the ESP32 integrated SPI flash memory – not recommended to use).
    GPIO 14
    GPIO 15



2019-09-14
----------

ESP32 :: Lolin32 GPIO pin numbers

C/D:    25  // (Command/Data)
WR:     26  // Falling edge latches
BUSY:   35	// INPUT
RESET:  27
EN:     14

// Address & data: Down the right-hand side
Address pins: {1, 3, 22, 21, 19, 23}
Data pins: {18, 5, 17, 16, 4, 15, 12, 13}

Power: 5V (orange) and GND (purple)




2019-09-09
----------
Trying to get this working with Teensy 3.2.

Teensy Pins:
0		C/D (control=0/data=1)
1		WR (falling edge latches Address and Data)
2		BUSY (1==busy with last operation, 0==ready)
3		RESET (0==resets device)
4		EN (1==enables display, 0==blank screen)
5-10	ADDRESS
13-20	DATA


A5 == teensy pin 5
A0 == teensy pin 0
... correct?
