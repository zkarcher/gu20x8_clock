<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="no" active="no"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="no" active="no"/>
<layer number="136" name="silktop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="LOLIN32">
<packages>
<package name="LOLIN32VAR1">
<wire x1="7.9" y1="31.726" x2="7.9" y2="25.926" width="0.127" layer="21"/>
<wire x1="-8.1" y1="25.926" x2="-8.1" y2="31.726" width="0.127" layer="21"/>
<wire x1="-8.1" y1="31.726" x2="7.9" y2="31.726" width="0.127" layer="21"/>
<wire x1="-8.1" y1="25.926" x2="7.9" y2="25.926" width="0.127" layer="21"/>
<pad name="GND@3" x="11.43" y="6.35" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GND@2" x="11.43" y="8.89" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO21/SDA" x="11.43" y="11.43" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO22/SCL" x="11.43" y="13.97" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="3V3@2" x="11.43" y="16.51" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="RX/0" x="11.43" y="19.05" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="TX/1" x="11.43" y="21.59" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GND@1" x="11.43" y="24.13" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO35/A7" x="-11.43" y="6.35" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO34/A6" x="-11.43" y="8.89" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO33/A5" x="-11.43" y="11.43" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO32/A4" x="-11.43" y="13.97" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO39/VN/A3" x="-11.43" y="16.51" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO36/VP/V0" x="-11.43" y="19.05" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="EN" x="-11.43" y="21.59" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="3V3@1" x="-11.43" y="24.13" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<text x="-3.81" y="10.16" size="1.4224" layer="21">WeMos.cc
LOLIN32</text>
<text x="0" y="-30.48" size="1.4224" layer="25" align="bottom-right">&gt;Name</text>
<wire x1="-8.1026" y1="25.9334" x2="-8.1026" y2="7.62" width="0.127" layer="21"/>
<wire x1="7.874" y1="25.9334" x2="7.8994" y2="25.9334" width="0.127" layer="21"/>
<wire x1="7.8994" y1="25.9334" x2="7.8994" y2="7.62" width="0.127" layer="21"/>
<wire x1="12.7" y1="29.21" x2="12.7" y2="-27.94" width="0.127" layer="21"/>
<wire x1="-10.16" y1="31.75" x2="10.16" y2="31.75" width="0.127" layer="21"/>
<wire x1="-12.7" y1="29.21" x2="-10.16" y2="31.75" width="0.127" layer="21" curve="-90"/>
<wire x1="10.16" y1="31.75" x2="12.7" y2="29.21" width="0.127" layer="21" curve="-90"/>
<wire x1="-12.7" y1="29.21" x2="-12.7" y2="-27.94" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-27.94" x2="12.7" y2="-27.94" width="0.127" layer="21"/>
<wire x1="-8.1026" y1="7.62" x2="7.874" y2="7.62" width="0.127" layer="21"/>
<text x="-10.16" y="24.13" size="0.762" layer="25" align="center-left">3V3</text>
<text x="-10.16" y="21.59" size="0.762" layer="25" align="center-left">EN</text>
<text x="-10.16" y="19.05" size="0.762" layer="25" align="center-left">GPIO36/VP/V0</text>
<text x="-10.16" y="16.51" size="0.762" layer="25" align="center-left">GPIO39/VN/A3</text>
<text x="-10.16" y="13.97" size="0.762" layer="25" align="center-left">GPIO32/A4</text>
<text x="-10.16" y="11.43" size="0.762" layer="25" align="center-left">GPIO33/A5</text>
<text x="-10.16" y="8.89" size="0.762" layer="25" align="center-left">GPIO34/A6</text>
<text x="-10.16" y="6.35" size="0.762" layer="25" align="center-left">GPIO35/A7</text>
<text x="10.16" y="6.35" size="0.762" layer="25" rot="R180" align="center-left">GND</text>
<text x="10.16" y="8.89" size="0.762" layer="25" rot="R180" align="center-left">GND</text>
<text x="10.16" y="11.43" size="0.762" layer="25" rot="R180" align="center-left">GPIO21/SDA</text>
<text x="10.16" y="13.97" size="0.762" layer="25" rot="R180" align="center-left">GPIO22/SCL</text>
<text x="10.16" y="16.51" size="0.762" layer="25" rot="R180" align="center-left">3V3</text>
<text x="10.16" y="19.05" size="0.762" layer="25" rot="R180" align="center-left">RX/0</text>
<text x="10.16" y="21.59" size="0.762" layer="25" rot="R180" align="center-left">TX/1</text>
<text x="10.16" y="24.13" size="0.762" layer="25" rot="R180" align="center-left">GND</text>
<wire x1="-3.81" y1="-26.67" x2="3.81" y2="-26.67" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-26.67" x2="3.81" y2="-22.86" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-22.86" x2="-3.81" y2="-22.86" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-22.86" x2="-3.81" y2="-26.67" width="0.2032" layer="21"/>
<text x="0" y="29.21" size="1.4224" layer="25" align="center">ANTENNA
KEEP CLEAR</text>
<text x="0" y="-24.13" size="1.016" layer="25" align="center">USB
MICRO</text>
<wire x1="-7.62" y1="-22.86" x2="-11.43" y2="-22.86" width="0.2032" layer="21"/>
<wire x1="-11.43" y1="-22.86" x2="-11.43" y2="-26.67" width="0.2032" layer="21"/>
<wire x1="-11.43" y1="-26.67" x2="-7.62" y2="-26.67" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="-26.67" x2="-7.62" y2="-22.86" width="0.2032" layer="21"/>
<text x="-9.525" y="-24.13" size="0.635" layer="25" align="center">RESET
SWITCH</text>
<pad name="GPIO4" x="11.43" y="-13.97" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO16" x="11.43" y="-11.43" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO17" x="11.43" y="-8.89" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="3V3@3" x="11.43" y="-6.35" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO5/LED/SS" x="11.43" y="-3.81" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO18/SCK" x="11.43" y="-1.27" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO23/MOSI" x="11.43" y="1.27" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO19/MISO" x="11.43" y="3.81" drill="1.016" diameter="1.8288" shape="long"/>
<text x="10.16" y="-13.97" size="0.762" layer="25" rot="R180" align="center-left">GPIO4</text>
<text x="10.16" y="-11.43" size="0.762" layer="25" rot="R180" align="center-left">GPIO16</text>
<text x="10.16" y="-8.89" size="0.762" layer="25" rot="R180" align="center-left">GPIO17</text>
<text x="10.16" y="-6.35" size="0.762" layer="25" rot="R180" align="center-left">3V3</text>
<text x="10.16" y="-3.81" size="0.762" layer="25" rot="R180" align="center-left">GPIO5/LED/SS</text>
<text x="10.16" y="-1.27" size="0.762" layer="25" rot="R180" align="center-left">GPIO18/SCK</text>
<text x="10.16" y="1.27" size="0.762" layer="25" rot="R180" align="center-left">GPIO23/MOSI</text>
<text x="10.16" y="3.81" size="0.762" layer="25" rot="R180" align="center-left">GPIO19/MISO</text>
<pad name="GPIO15" x="11.43" y="-24.13" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO2" x="11.43" y="-21.59" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GND@4" x="11.43" y="-19.05" drill="1.016" diameter="1.8288" shape="long"/>
<pad name="GPIO0" x="11.43" y="-16.51" drill="1.016" diameter="1.8288" shape="long"/>
<text x="8.89" y="-24.13" size="0.762" layer="25" rot="R180" align="center-left">GPIO15</text>
<text x="8.89" y="-21.59" size="0.762" layer="25" rot="R180" align="center-left">GPIO2</text>
<text x="8.89" y="-19.05" size="0.762" layer="25" rot="R180" align="center-left">GND</text>
<text x="8.89" y="-16.51" size="0.762" layer="25" rot="R180" align="center-left">GPIO0</text>
<pad name="GND@5" x="-11.43" y="-13.97" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="5V" x="-11.43" y="-11.43" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO13/A14" x="-11.43" y="-8.89" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO12/A15" x="-11.43" y="-6.35" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO14/A16" x="-11.43" y="-3.81" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO27/A17" x="-11.43" y="-1.27" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO26/DAC2/A19" x="-11.43" y="1.27" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<pad name="GPIO25/DAC1/A18" x="-11.43" y="3.81" drill="1.016" diameter="1.8288" shape="long" rot="R180"/>
<text x="-10.16" y="3.81" size="0.762" layer="25" align="center-left">GPIO25/DAC1/A18</text>
<text x="-10.16" y="1.27" size="0.762" layer="25" align="center-left">GPIO26/DAC2/A19</text>
<text x="-10.16" y="-1.27" size="0.762" layer="25" align="center-left">GPIO27/A17</text>
<text x="-10.16" y="-3.81" size="0.762" layer="25" align="center-left">GPIO14/A16</text>
<text x="-10.16" y="-6.35" size="0.762" layer="25" align="center-left">GPIO12/A15</text>
<text x="-10.16" y="-8.89" size="0.762" layer="25" align="center-left">GPIO13/A14</text>
<text x="-10.16" y="-11.43" size="0.762" layer="25" align="center-left">5V</text>
<text x="-8.89" y="-13.97" size="0.762" layer="25" align="center-left">GND</text>
</package>
<package name="DS3231BREAKOUT">
<pad name="SCL" x="5.08" y="10.16" drill="0.9" diameter="2.032" shape="square"/>
<pad name="SDA" x="5.08" y="7.62" drill="0.9" diameter="2.032" shape="octagon"/>
<pad name="VCC@1" x="5.08" y="5.08" drill="0.9" diameter="2.032" shape="octagon"/>
<pad name="GND@1" x="5.08" y="2.54" drill="0.9" diameter="2.032" shape="octagon"/>
<wire x1="3.81" y1="11.43" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="6.35" y2="1.27" width="0.127" layer="21"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="11.43" width="0.127" layer="21"/>
<wire x1="6.35" y1="11.43" x2="3.81" y2="11.43" width="0.127" layer="21"/>
<text x="7.62" y="1.27" size="1.27" layer="21" rot="R90">DS3231 RTC</text>
<text x="0" y="10.16" size="1.27" layer="21">SCL</text>
<text x="0" y="7.62" size="1.27" layer="21">SDA</text>
<text x="0" y="5.08" size="1.27" layer="21">VCC</text>
<text x="0" y="2.54" size="1.27" layer="21">GND</text>
</package>
<package name="MAX9814BREAKOUT">
<pad name="AR" x="1.27" y="11.43" drill="0.9" diameter="2.1844" shape="octagon"/>
<pad name="OUT" x="1.27" y="8.89" drill="0.9" diameter="2.1844" shape="octagon"/>
<pad name="GAIN" x="1.27" y="6.35" drill="0.9" diameter="2.1844" shape="octagon"/>
<pad name="VDD" x="1.27" y="3.81" drill="0.9" diameter="2.1844" shape="square"/>
<pad name="GND" x="1.27" y="1.27" drill="0.9" diameter="2.1844" shape="octagon" first="yes"/>
<text x="-2.54" y="0.635" size="0.8128" layer="21">GND</text>
<wire x1="0" y1="12.7" x2="0" y2="5.08" width="0.127" layer="21"/>
<wire x1="0" y1="5.08" x2="0" y2="2.54" width="0.127" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="12.7" width="0.127" layer="21"/>
<wire x1="2.54" y1="12.7" x2="0" y2="12.7" width="0.127" layer="21"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<text x="-2.54" y="3.175" size="0.8128" layer="21">VDD</text>
<text x="-2.54" y="5.715" size="0.8128" layer="21">GAIN</text>
<text x="-2.54" y="8.255" size="0.8128" layer="21">OUT</text>
<text x="-2.54" y="10.795" size="0.8128" layer="21">AR</text>
<text x="3.81" y="2.54" size="1.27" layer="21" rot="R90">MAX9814</text>
</package>
</packages>
<symbols>
<symbol name="LOLIN32">
<pin name="GPIO26/DAC2/A19" x="50.8" y="22.86" length="middle" rot="R180"/>
<pin name="GPIO25/DAC1/A18" x="50.8" y="25.4" length="middle" rot="R180"/>
<pin name="GPIO17" x="50.8" y="27.94" length="middle" rot="R180"/>
<pin name="GPIO16" x="50.8" y="30.48" length="middle" rot="R180"/>
<pin name="GPIO15" x="50.8" y="33.02" length="middle" rot="R180"/>
<pin name="GPIO14/A16" x="50.8" y="35.56" length="middle" rot="R180"/>
<pin name="GPIO13/A14" x="50.8" y="38.1" length="middle" rot="R180"/>
<pin name="5V" x="0" y="58.42" length="middle" direction="pwr"/>
<pin name="SCL/GPIO22" x="0" y="35.56" length="middle"/>
<pin name="SDA/GPIO21" x="0" y="33.02" length="middle"/>
<pin name="TX" x="0" y="27.94" length="middle" direction="out"/>
<pin name="RX" x="0" y="25.4" length="middle" direction="in"/>
<pin name="GND" x="0" y="2.54" length="middle" direction="pwr"/>
<pin name="MOSI/GPIO23" x="0" y="43.18" length="middle"/>
<pin name="MISO/GPIO19" x="0" y="40.64" length="middle"/>
<pin name="SCK/GPIO18" x="0" y="45.72" length="middle"/>
<pin name="SS/LED/GPIO5" x="0" y="48.26" length="middle"/>
<pin name="EN" x="0" y="22.86" length="middle"/>
<pin name="GPIO0" x="50.8" y="48.26" length="middle" rot="R180"/>
<pin name="GPIO2" x="50.8" y="45.72" length="middle" rot="R180"/>
<pin name="GPIO4" x="50.8" y="43.18" length="middle" rot="R180"/>
<pin name="GPIO12/A13" x="50.8" y="40.64" length="middle" rot="R180"/>
<pin name="GPIO27/A17" x="50.8" y="20.32" length="middle" rot="R180"/>
<pin name="GPIO32/A4" x="50.8" y="17.78" length="middle" rot="R180"/>
<pin name="GPIO33/A5" x="50.8" y="15.24" length="middle" rot="R180"/>
<pin name="GPIO34/A6" x="50.8" y="12.7" length="middle" direction="in" rot="R180"/>
<pin name="GPIO35/A7" x="50.8" y="10.16" length="middle" direction="in" rot="R180"/>
<pin name="GPIO36/A0/VP" x="50.8" y="7.62" length="middle" direction="in" rot="R180"/>
<pin name="GPIO39/A3/VN" x="50.8" y="5.08" length="middle" direction="in" rot="R180"/>
<wire x1="5.08" y1="60.96" x2="45.72" y2="60.96" width="0.254" layer="94"/>
<wire x1="45.72" y1="60.96" x2="45.72" y2="0" width="0.254" layer="94"/>
<wire x1="45.72" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="60.96" width="0.254" layer="94"/>
<pin name="3.3V" x="50.8" y="58.42" length="middle" direction="pwr" rot="R180"/>
<text x="7.62" y="63.5" size="1.778" layer="95">&gt;NAME</text>
<text x="7.62" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DS3231BREAKOUT">
<description>DS3231 I2C RTC  module with AT24C32</description>
<pin name="VCC" x="5.08" y="10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="SCL" x="5.08" y="7.62" length="middle" rot="R180"/>
<pin name="SDA" x="5.08" y="5.08" length="middle" rot="R180"/>
<pin name="GND" x="5.08" y="2.54" length="middle" direction="pwr" rot="R180"/>
<wire x1="2.54" y1="15.24" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="17.78" y1="15.24" x2="2.54" y2="15.24" width="0.254" layer="94"/>
<text x="7.62" y="12.7" size="1.778" layer="94">DS3231</text>
<text x="12.7" y="10.16" size="1.778" layer="94">RTC</text>
</symbol>
<symbol name="MAX9814BREAKOUT">
<description>MAX9814 sound sensor breakout board</description>
<pin name="VDD" x="7.62" y="12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="OUT" x="7.62" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="GAIN" x="7.62" y="7.62" length="middle" direction="in" rot="R180"/>
<pin name="AR" x="7.62" y="5.08" length="middle" direction="in" rot="R180"/>
<pin name="GND" x="7.62" y="2.54" length="middle" direction="pwr" rot="R180"/>
<wire x1="5.08" y1="15.24" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="5.08" y2="15.24" width="0.254" layer="94"/>
<text x="12.7" y="5.08" size="1.27" layer="94" rot="R90">MAX9814</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="WEMOSLOLIN32">
<description>WEMOS LOLIN32 CPU with Wifi and 4Gb flash storage</description>
<gates>
<gate name="G$1" symbol="LOLIN32" x="38.1" y="2.54"/>
</gates>
<devices>
<device name="" package="LOLIN32VAR1">
<connects>
<connect gate="G$1" pin="3.3V" pad="3V3@1 3V3@2 3V3@3" route="any"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="GND" pad="GND@1 GND@2 GND@3 GND@4 GND@5" route="any"/>
<connect gate="G$1" pin="GPIO0" pad="GPIO0"/>
<connect gate="G$1" pin="GPIO12/A13" pad="GPIO12/A15"/>
<connect gate="G$1" pin="GPIO13/A14" pad="GPIO13/A14"/>
<connect gate="G$1" pin="GPIO14/A16" pad="GPIO14/A16"/>
<connect gate="G$1" pin="GPIO15" pad="GPIO15"/>
<connect gate="G$1" pin="GPIO16" pad="GPIO16"/>
<connect gate="G$1" pin="GPIO17" pad="GPIO17"/>
<connect gate="G$1" pin="GPIO2" pad="GPIO2"/>
<connect gate="G$1" pin="GPIO25/DAC1/A18" pad="GPIO25/DAC1/A18"/>
<connect gate="G$1" pin="GPIO26/DAC2/A19" pad="GPIO26/DAC2/A19"/>
<connect gate="G$1" pin="GPIO27/A17" pad="GPIO27/A17"/>
<connect gate="G$1" pin="GPIO32/A4" pad="GPIO32/A4"/>
<connect gate="G$1" pin="GPIO33/A5" pad="GPIO33/A5"/>
<connect gate="G$1" pin="GPIO34/A6" pad="GPIO34/A6"/>
<connect gate="G$1" pin="GPIO35/A7" pad="GPIO35/A7"/>
<connect gate="G$1" pin="GPIO36/A0/VP" pad="GPIO36/VP/V0"/>
<connect gate="G$1" pin="GPIO39/A3/VN" pad="GPIO39/VN/A3"/>
<connect gate="G$1" pin="GPIO4" pad="GPIO4"/>
<connect gate="G$1" pin="MISO/GPIO19" pad="GPIO19/MISO"/>
<connect gate="G$1" pin="MOSI/GPIO23" pad="GPIO23/MOSI"/>
<connect gate="G$1" pin="RX" pad="RX/0"/>
<connect gate="G$1" pin="SCK/GPIO18" pad="GPIO18/SCK"/>
<connect gate="G$1" pin="SCL/GPIO22" pad="GPIO22/SCL"/>
<connect gate="G$1" pin="SDA/GPIO21" pad="GPIO21/SDA"/>
<connect gate="G$1" pin="SS/LED/GPIO5" pad="GPIO5/LED/SS"/>
<connect gate="G$1" pin="TX" pad="TX/1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DS3231BREAKOUT">
<gates>
<gate name="G$1" symbol="DS3231BREAKOUT" x="-5.08" y="2.54"/>
</gates>
<devices>
<device name="" package="DS3231BREAKOUT">
<connects>
<connect gate="G$1" pin="GND" pad="GND@1"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="VCC" pad="VCC@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MAX9814BREAKOUT">
<gates>
<gate name="G$1" symbol="MAX9814BREAKOUT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MAX9814BREAKOUT">
<connects>
<connect gate="G$1" pin="AR" pad="AR"/>
<connect gate="G$1" pin="GAIN" pad="GAIN"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="OUT" pad="OUT"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SN74HC164N">
<packages>
<package name="DIP254P762X508-14">
<wire x1="-0.508" y1="16.4592" x2="-0.508" y2="17.4752" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="-1.2192" x2="-7.112" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="-2.2352" x2="-0.508" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-2.2352" x2="-0.508" y2="-1.2192" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="17.4752" x2="-3.5052" y2="17.4752" width="0.1524" layer="21"/>
<wire x1="-3.5052" y1="17.4752" x2="-4.1148" y2="17.4752" width="0.1524" layer="21"/>
<wire x1="-4.1148" y1="17.4752" x2="-7.112" y2="17.4752" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="17.4752" x2="-7.112" y2="16.5608" width="0.1524" layer="21"/>
<wire x1="-3.5052" y1="17.4752" x2="-4.1148" y2="17.4752" width="0" layer="21" curve="-180"/>
<wire x1="-7.112" y1="14.6812" x2="-7.112" y2="15.7988" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="15.7988" x2="-8.1788" y2="15.7988" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="15.7988" x2="-8.1788" y2="14.6812" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="14.6812" x2="-7.112" y2="14.6812" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="12.1412" x2="-7.112" y2="13.2588" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="13.2588" x2="-8.1788" y2="13.2588" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="13.2588" x2="-8.1788" y2="12.1412" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="12.1412" x2="-7.112" y2="12.1412" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="9.6012" x2="-7.112" y2="10.7188" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="10.7188" x2="-8.1788" y2="10.7188" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="10.7188" x2="-8.1788" y2="9.6012" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="9.6012" x2="-7.112" y2="9.6012" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="7.0612" x2="-7.112" y2="8.1788" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="8.1788" x2="-8.1788" y2="8.1788" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="8.1788" x2="-8.1788" y2="7.0612" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="7.0612" x2="-7.112" y2="7.0612" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="4.5212" x2="-7.112" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="5.6388" x2="-8.1788" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="5.6388" x2="-8.1788" y2="4.5212" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="4.5212" x2="-7.112" y2="4.5212" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="1.9812" x2="-7.112" y2="3.0988" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="3.0988" x2="-8.1788" y2="3.0988" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="3.0988" x2="-8.1788" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="1.9812" x2="-7.112" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-0.5588" x2="-7.112" y2="0.5588" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="0.5588" x2="-8.1788" y2="0.5588" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="0.5588" x2="-8.1788" y2="-0.5588" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="-0.5588" x2="-7.112" y2="-0.5588" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.5588" x2="-0.508" y2="-0.5588" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="-0.5588" x2="0.5588" y2="-0.5588" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="-0.5588" x2="0.5588" y2="0.5588" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="0.5588" x2="-0.508" y2="0.5588" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="3.0988" x2="-0.508" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="1.9812" x2="0.5588" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="1.9812" x2="0.5588" y2="3.0988" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="3.0988" x2="-0.508" y2="3.0988" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="5.6388" x2="-0.508" y2="4.5212" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="4.5212" x2="0.5588" y2="4.5212" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="4.5212" x2="0.5588" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="5.6388" x2="-0.508" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="8.1788" x2="-0.508" y2="7.0612" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="7.0612" x2="0.5588" y2="7.0612" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="7.0612" x2="0.5588" y2="8.1788" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="8.1788" x2="-0.508" y2="8.1788" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="10.7188" x2="-0.508" y2="9.6012" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="9.6012" x2="0.5588" y2="9.6012" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="9.6012" x2="0.5588" y2="10.7188" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="10.7188" x2="-0.508" y2="10.7188" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="13.2588" x2="-0.508" y2="12.1412" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="12.1412" x2="0.5588" y2="12.1412" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="12.1412" x2="0.5588" y2="13.2588" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="13.2588" x2="-0.508" y2="13.2588" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="15.7988" x2="-0.508" y2="14.6812" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="14.6812" x2="0.5588" y2="14.6812" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="14.6812" x2="0.5588" y2="15.7988" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="15.7988" x2="-0.508" y2="15.7988" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="-2.2352" x2="-0.508" y2="-2.2352" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="-2.2352" x2="-0.508" y2="17.4752" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="17.4752" x2="-3.5052" y2="17.4752" width="0.1524" layer="51"/>
<wire x1="-3.5052" y1="17.4752" x2="-4.1148" y2="17.4752" width="0.1524" layer="51"/>
<wire x1="-4.1148" y1="17.4752" x2="-7.112" y2="17.4752" width="0.1524" layer="51"/>
<wire x1="-7.112" y1="17.4752" x2="-7.112" y2="-2.2352" width="0.1524" layer="51"/>
<wire x1="-3.5052" y1="17.4752" x2="-4.1148" y2="17.4752" width="0" layer="51" curve="-180"/>
<text x="-8.61963125" y="18.2817" size="2.08498125" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-9.64496875" y="-4.911559375" size="2.08678125" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<pad name="1" x="-7.62" y="15.24" drill="1.1176" shape="square"/>
<pad name="2" x="-7.62" y="12.7" drill="1.1176"/>
<pad name="3" x="-7.62" y="10.16" drill="1.1176"/>
<pad name="4" x="-7.62" y="7.62" drill="1.1176"/>
<pad name="5" x="-7.62" y="5.08" drill="1.1176"/>
<pad name="6" x="-7.62" y="2.54" drill="1.1176"/>
<pad name="7" x="-7.62" y="0" drill="1.1176"/>
<pad name="8" x="0" y="0" drill="1.1176"/>
<pad name="9" x="0" y="2.54" drill="1.1176"/>
<pad name="10" x="0" y="5.08" drill="1.1176"/>
<pad name="11" x="0" y="7.62" drill="1.1176"/>
<pad name="12" x="0" y="10.16" drill="1.1176"/>
<pad name="13" x="0" y="12.7" drill="1.1176"/>
<pad name="14" x="0" y="15.24" drill="1.1176"/>
</package>
</packages>
<symbols>
<symbol name="SN74HC164N">
<wire x1="-12.7" y1="12.7" x2="-12.7" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="12.7" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="12.7" width="0.4064" layer="94"/>
<wire x1="12.7" y1="12.7" x2="-12.7" y2="12.7" width="0.4064" layer="94"/>
<text x="-5.35988125" y="16.7401" size="2.082990625" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.310690625" y="-23.0469" size="2.08361875" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="VCC" x="-17.78" y="7.62" length="middle" direction="pwr"/>
<pin name="A" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="B" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="CLK" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="~CLR" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="GND" x="-17.78" y="-12.7" length="middle" direction="pas"/>
<pin name="QA" x="17.78" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="QB" x="17.78" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="QC" x="17.78" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="QD" x="17.78" y="0" length="middle" direction="out" rot="R180"/>
<pin name="QE" x="17.78" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="QF" x="17.78" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="QG" x="17.78" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="QH" x="17.78" y="-10.16" length="middle" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SN74HC164N" prefix="U">
<description>8-BIT PARALLEL-OUT SERIAL SHIFT REGISTERS</description>
<gates>
<gate name="A" symbol="SN74HC164N" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIP254P762X508-14">
<connects>
<connect gate="A" pin="A" pad="1"/>
<connect gate="A" pin="B" pad="2"/>
<connect gate="A" pin="CLK" pad="8"/>
<connect gate="A" pin="GND" pad="7"/>
<connect gate="A" pin="QA" pad="3"/>
<connect gate="A" pin="QB" pad="4"/>
<connect gate="A" pin="QC" pad="5"/>
<connect gate="A" pin="QD" pad="6"/>
<connect gate="A" pin="QE" pad="10"/>
<connect gate="A" pin="QF" pad="11"/>
<connect gate="A" pin="QG" pad="12"/>
<connect gate="A" pin="QH" pad="13"/>
<connect gate="A" pin="VCC" pad="14"/>
<connect gate="A" pin="~CLR" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Shift Register Single 8-Bit Serial to Parallel 14-Pin PDIP Tube "/>
<attribute name="MF" value="Texas Instruments"/>
<attribute name="MP" value="SN74HC164N"/>
<attribute name="PACKAGE" value="DIP-14 Texas Instruments"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="KAYTA9000">
<packages>
<package name="VFD_GU20X8">
<pad name="ADDR2" x="-1.27" y="0" drill="1" diameter="1.6764"/>
<pad name="ADDR3" x="1.27" y="0" drill="1" diameter="1.6764"/>
<pad name="ADDR1" x="1.27" y="-2.54" drill="1" diameter="1.6764"/>
<pad name="ADDR0" x="-1.27" y="-2.54" drill="1" diameter="1.6764"/>
<pad name="ADDR4" x="-1.27" y="2.54" drill="1" diameter="1.6764"/>
<pad name="ADDR5" x="1.27" y="2.54" drill="1" diameter="1.6764"/>
<pad name="NC_12" x="1.27" y="-5.08" drill="1" diameter="1.6764"/>
<pad name="NC_11" x="-1.27" y="-5.08" drill="1" diameter="1.6764"/>
<pad name="DATA0" x="-1.27" y="5.08" drill="1" diameter="1.6764"/>
<pad name="DATA1" x="1.27" y="5.08" drill="1" diameter="1.6764"/>
<pad name="NC_10" x="1.27" y="-7.62" drill="1" diameter="1.6764"/>
<pad name="NC_9" x="-1.27" y="-7.62" drill="1" diameter="1.6764"/>
<pad name="DATA2" x="-1.27" y="7.62" drill="1" diameter="1.6764"/>
<pad name="DATA3" x="1.27" y="7.62" drill="1" diameter="1.6764"/>
<pad name="NC_8" x="1.27" y="-10.16" drill="1" diameter="1.6764"/>
<pad name="EN" x="-1.27" y="-10.16" drill="1" diameter="1.6764"/>
<pad name="DATA4" x="-1.27" y="10.16" drill="1" diameter="1.6764"/>
<pad name="DATA5" x="1.27" y="10.16" drill="1" diameter="1.6764"/>
<pad name="RST" x="1.27" y="-12.7" drill="1" diameter="1.6764"/>
<pad name="BUSY" x="-1.27" y="-12.7" drill="1" diameter="1.6764"/>
<pad name="DATA6" x="-1.27" y="12.7" drill="1" diameter="1.6764"/>
<pad name="DATA7" x="1.27" y="12.7" drill="1" diameter="1.6764"/>
<pad name="NC_28" x="1.27" y="15.24" drill="1" diameter="1.6764"/>
<pad name="NC_27" x="-1.27" y="15.24" drill="1" diameter="1.6764"/>
<pad name="NC_29" x="-1.27" y="17.78" drill="1" diameter="1.6764"/>
<pad name="NC_30" x="1.27" y="17.78" drill="1" diameter="1.6764"/>
<pad name="WR" x="-1.27" y="-15.24" drill="1" diameter="1.6764"/>
<pad name="NC_4" x="1.27" y="-15.24" drill="1" diameter="1.6764"/>
<pad name="NC_2" x="1.27" y="-17.78" drill="1" diameter="1.6764"/>
<pad name="C/D" x="-1.27" y="-17.78" drill="1" diameter="1.6764" shape="square"/>
<text x="3.81" y="-12.7" size="1.27" layer="21" font="vector" align="center-left">RS</text>
<text x="3.81" y="-2.54" size="1.27" layer="21" font="vector" align="center-left">A1</text>
<text x="3.81" y="0" size="1.27" layer="21" font="vector" align="center-left">A3</text>
<text x="3.81" y="2.54" size="1.27" layer="21" font="vector" align="center-left">A5</text>
<text x="3.81" y="5.08" size="1.27" layer="21" font="vector" align="center-left">D1</text>
<text x="3.81" y="7.62" size="1.27" layer="21" font="vector" align="center-left">D3</text>
<text x="3.81" y="10.16" size="1.27" layer="21" font="vector" align="center-left">D5</text>
<text x="3.81" y="12.7" size="1.27" layer="21" font="vector" align="center-left">D7</text>
<text x="-3.81" y="-17.78" size="1.27" layer="21" font="vector" align="center-right">CD</text>
<text x="-3.81" y="-15.24" size="1.27" layer="21" font="vector" align="center-right">WR</text>
<text x="-3.81" y="-12.7" size="1.27" layer="21" font="vector" align="center-right">BS</text>
<text x="-3.81" y="-10.16" size="1.27" layer="21" font="vector" align="center-right">EN</text>
<text x="-3.81" y="-2.54" size="1.27" layer="21" font="vector" align="center-right">A0</text>
<text x="-3.81" y="0" size="1.27" layer="21" font="vector" align="center-right">A2</text>
<text x="-3.81" y="2.54" size="1.27" layer="21" font="vector" align="center-right">A4</text>
<text x="-3.81" y="5.08" size="1.27" layer="21" font="vector" align="center-right">D0</text>
<text x="-3.81" y="7.62" size="1.27" layer="21" font="vector" align="center-right">D2</text>
<text x="-3.81" y="10.16" size="1.27" layer="21" font="vector" align="center-right">D4</text>
<text x="-3.81" y="12.7" size="1.27" layer="21" font="vector" align="center-right">D6</text>
<text x="-1.27" y="-20.32" size="1.27" layer="21" font="vector" rot="R90" align="center">1</text>
<text x="1.27" y="-20.32" size="1.27" layer="21" rot="R90" align="center">1</text>
<text x="1.27" y="-20.32" size="1.27" layer="21" font="vector" rot="R90" align="center">2</text>
<text x="-1.27" y="20.32" size="1.27" layer="21" font="vector" rot="R90" align="center">29</text>
<text x="1.27" y="20.32" size="1.27" layer="21" font="vector" rot="R90" align="center">30</text>
<wire x1="-2.54" y1="19.05" x2="-2.54" y2="-19.05" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-19.05" x2="2.54" y2="-19.05" width="0.127" layer="21"/>
<wire x1="2.54" y1="-19.05" x2="2.54" y2="19.05" width="0.127" layer="21"/>
<wire x1="2.54" y1="19.05" x2="-2.54" y2="19.05" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="VFD_GU20X8">
<pin name="C/D" x="-17.78" y="25.4" length="middle" direction="in"/>
<pin name="WR" x="-17.78" y="22.86" length="middle" direction="in"/>
<pin name="BUSY" x="17.78" y="17.78" length="middle" direction="out" rot="R180"/>
<pin name="RST" x="-17.78" y="20.32" length="middle" direction="in"/>
<pin name="EN" x="-17.78" y="17.78" length="middle" direction="in"/>
<pin name="NC_2" x="17.78" y="5.08" length="middle" direction="nc" rot="R180"/>
<pin name="NC_4" x="17.78" y="2.54" length="middle" direction="nc" rot="R180"/>
<pin name="NC_8" x="17.78" y="0" length="middle" direction="nc" rot="R180"/>
<pin name="NC_9" x="17.78" y="-2.54" length="middle" direction="nc" rot="R180"/>
<pin name="NC_10" x="17.78" y="-5.08" length="middle" direction="nc" rot="R180"/>
<pin name="NC_11" x="17.78" y="-7.62" length="middle" direction="nc" rot="R180"/>
<pin name="NC_12" x="17.78" y="-10.16" length="middle" direction="nc" rot="R180"/>
<pin name="NC_27" x="17.78" y="-12.7" length="middle" direction="nc" rot="R180"/>
<pin name="NC_28" x="17.78" y="-15.24" length="middle" direction="nc" rot="R180"/>
<pin name="NC_29" x="17.78" y="-17.78" length="middle" direction="nc" rot="R180"/>
<pin name="NC_30" x="17.78" y="-20.32" length="middle" direction="nc" rot="R180"/>
<pin name="DATA7" x="-17.78" y="-25.4" length="middle" direction="in"/>
<pin name="DATA6" x="-17.78" y="-22.86" length="middle" direction="in"/>
<pin name="DATA5" x="-17.78" y="-20.32" length="middle" direction="in"/>
<pin name="DATA4" x="-17.78" y="-17.78" length="middle" direction="in"/>
<pin name="DATA3" x="-17.78" y="-15.24" length="middle" direction="in"/>
<pin name="DATA2" x="-17.78" y="-12.7" length="middle" direction="in"/>
<pin name="DATA1" x="-17.78" y="-10.16" length="middle" direction="in"/>
<pin name="DATA0" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="ADDR5" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="ADDR4" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="ADDR3" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="ADDR2" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="ADDR1" x="-17.78" y="7.62" length="middle" direction="in"/>
<pin name="ADDR0" x="-17.78" y="10.16" length="middle" direction="in"/>
<wire x1="-12.7" y1="27.94" x2="12.7" y2="27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="27.94" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="-12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-27.94" x2="-12.7" y2="27.94" width="0.254" layer="94"/>
<text x="0" y="30.48" size="1.27" layer="94" align="bottom-center">VFD_GU20X8</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="VFD_GU20X8">
<gates>
<gate name="G$1" symbol="VFD_GU20X8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VFD_GU20X8">
<connects>
<connect gate="G$1" pin="ADDR0" pad="ADDR0"/>
<connect gate="G$1" pin="ADDR1" pad="ADDR1"/>
<connect gate="G$1" pin="ADDR2" pad="ADDR2"/>
<connect gate="G$1" pin="ADDR3" pad="ADDR3"/>
<connect gate="G$1" pin="ADDR4" pad="ADDR4"/>
<connect gate="G$1" pin="ADDR5" pad="ADDR5"/>
<connect gate="G$1" pin="BUSY" pad="BUSY"/>
<connect gate="G$1" pin="C/D" pad="C/D"/>
<connect gate="G$1" pin="DATA0" pad="DATA0"/>
<connect gate="G$1" pin="DATA1" pad="DATA1"/>
<connect gate="G$1" pin="DATA2" pad="DATA2"/>
<connect gate="G$1" pin="DATA3" pad="DATA3"/>
<connect gate="G$1" pin="DATA4" pad="DATA4"/>
<connect gate="G$1" pin="DATA5" pad="DATA5"/>
<connect gate="G$1" pin="DATA6" pad="DATA6"/>
<connect gate="G$1" pin="DATA7" pad="DATA7"/>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="NC_10" pad="NC_10"/>
<connect gate="G$1" pin="NC_11" pad="NC_11"/>
<connect gate="G$1" pin="NC_12" pad="NC_12"/>
<connect gate="G$1" pin="NC_2" pad="NC_2"/>
<connect gate="G$1" pin="NC_27" pad="NC_27"/>
<connect gate="G$1" pin="NC_28" pad="NC_28"/>
<connect gate="G$1" pin="NC_29" pad="NC_29"/>
<connect gate="G$1" pin="NC_30" pad="NC_30"/>
<connect gate="G$1" pin="NC_4" pad="NC_4"/>
<connect gate="G$1" pin="NC_8" pad="NC_8"/>
<connect gate="G$1" pin="NC_9" pad="NC_9"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="WR" pad="WR"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="wirepad" urn="urn:adsk.eagle:library:412">
<description>&lt;b&gt;Single Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2,54/1,0" urn="urn:adsk.eagle:footprint:30810/1" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1" size="0.0254" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="2,54/1,0" urn="urn:adsk.eagle:package:30828/1" type="box" library_version="2">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,54/1,0"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PAD" urn="urn:adsk.eagle:symbol:30808/1" library_version="2">
<wire x1="-1.016" y1="1.016" x2="1.016" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" width="0.254" layer="94"/>
<text x="-1.143" y="1.8542" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.143" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2,54/1,0" urn="urn:adsk.eagle:component:30846/2" prefix="PAD" uservalue="yes" library_version="2">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<gates>
<gate name="P" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2,54/1,0">
<connects>
<connect gate="P" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30828/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="OPL_Capacitor" urn="urn:adsk.eagle:library:8004261">
<description>&lt;b&gt;Seeed Open Parts Library (OPL) for the Seeed Fusion PCB Assembly Service
&lt;br&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com/opl.html" title="https://www.seeedstudio.com/opl.html"&gt;Seeed Fusion PCBA OPL&lt;/a&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com/fusion_pcb.html"&gt;Order PCB/PCBA&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com"&gt;www.seeedstudio.com&lt;/a&gt;
&lt;br&gt;&lt;/b&gt;</description>
<packages>
<package name="PC-D5.3MM" urn="urn:adsk.eagle:footprint:8004263/1" library_version="3">
<circle x="0" y="0" radius="2.65" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.694" width="0.127" layer="39"/>
<pad name="+" x="-1" y="0" drill="0.8" diameter="1.35" shape="square"/>
<pad name="-" x="1" y="0" drill="0.8" diameter="1.35"/>
<text x="-1.905" y="2.921" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.524" y="-4.191" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-4.445" y1="0" x2="-3.81" y2="0" width="0.127" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.81" y2="0" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="0.635" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0" y1="2.667" x2="0" y2="1.651" width="0.127" layer="21"/>
<wire x1="0" y1="1.651" x2="0" y2="1.016" width="0.127" layer="21"/>
<wire x1="0" y1="1.016" x2="0" y2="0.381" width="0.127" layer="21"/>
<wire x1="0" y1="0.381" x2="0" y2="-1.524" width="0.127" layer="21"/>
<wire x1="0" y1="-1.524" x2="0" y2="-2.286" width="0.127" layer="21"/>
<wire x1="0" y1="-2.286" x2="0" y2="-2.667" width="0.127" layer="21"/>
<wire x1="0" y1="-2.286" x2="2.286" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.524" x2="2.54" y2="-0.381" width="0.127" layer="21"/>
<wire x1="1.5875" y1="0.635" x2="2.413" y2="1.016" width="0.127" layer="21"/>
<wire x1="0" y1="0.381" x2="2.159" y2="1.524" width="0.127" layer="21"/>
<wire x1="0" y1="1.016" x2="1.778" y2="1.9685" width="0.127" layer="21"/>
<wire x1="0" y1="1.651" x2="1.27" y2="2.3495" width="0.127" layer="21"/>
<wire x1="1.75" y1="0" x2="2.413" y2="0.381" width="0.127" layer="21"/>
</package>
<package name="CERAMIC-2.54" urn="urn:adsk.eagle:footprint:8004267/1" library_version="3">
<wire x1="-2.54" y1="1.143" x2="2.54" y2="1.143" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.143" x2="2.54" y2="-1.143" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.143" x2="-2.54" y2="-1.143" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.143" x2="-2.54" y2="1.143" width="0.127" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.635" diameter="1.143"/>
<pad name="2" x="1.27" y="0" drill="0.635" diameter="1.143"/>
<text x="-1.905" y="1.27" size="0.8128" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.651" y="-1.651" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.016" y="-0.254" size="0.4064" layer="33" ratio="10">&gt;NAME</text>
<rectangle x1="-2.54" y1="-1.143" x2="2.54" y2="1.143" layer="39"/>
</package>
</packages>
<packages3d>
<package3d name="PC-D5.3MM" urn="urn:adsk.eagle:package:8004277/1" type="box" library_version="3">
<packageinstances>
<packageinstance name="PC-D5.3MM"/>
</packageinstances>
</package3d>
<package3d name="CERAMIC-2.54" urn="urn:adsk.eagle:package:8004281/1" type="box" library_version="3">
<packageinstances>
<packageinstance name="CERAMIC-2.54"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CAP-POLAR" urn="urn:adsk.eagle:symbol:8004274/1" library_version="3">
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.508" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.508" layer="94" curve="47.924978"/>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="0" size="1.27" layer="93" ratio="10">+</text>
<text x="-6.35" y="3.81" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="1.27" y="3.81" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<text x="1.27" y="0" size="1.27" layer="93" ratio="10">-</text>
<pin name="+" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="C" urn="urn:adsk.eagle:symbol:8004275/1" library_version="3">
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="3.81" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIP-ALUMINUM-100UF-16V(D5-H7MM)" urn="urn:adsk.eagle:component:8004360/1" prefix="C" uservalue="yes" library_version="3">
<description>302030031</description>
<gates>
<gate name="G$1" symbol="CAP-POLAR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PC-D5.3MM">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8004277/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MPN" value="KS 100uF/16V" constant="no"/>
<attribute name="VALUE" value="100uf"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIP-CERAMIC-DISC-10NF-50V-Y5V(D4.0MM)" urn="urn:adsk.eagle:component:8004334/1" prefix="C" uservalue="yes" library_version="3">
<description>302010202</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CERAMIC-2.54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8004281/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="VALUE" value="10nf"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
<clearance class="0" value="0.4064"/>
</class>
<class number="1" name="power" width="0.8128" drill="0">
<clearance class="0" value="0.4064"/>
<clearance class="1" value="0.4064"/>
</class>
</classes>
<parts>
<part name="U$1" library="LOLIN32" deviceset="WEMOSLOLIN32" device=""/>
<part name="SHIFT_REG_DATA" library="SN74HC164N" deviceset="SN74HC164N" device=""/>
<part name="U$2" library="KAYTA9000" deviceset="VFD_GU20X8" device=""/>
<part name="C2" library="OPL_Capacitor" library_urn="urn:adsk.eagle:library:8004261" deviceset="DIP-ALUMINUM-100UF-16V(D5-H7MM)" device="" package3d_urn="urn:adsk.eagle:package:8004277/1" value="10uf"/>
<part name="C4" library="OPL_Capacitor" library_urn="urn:adsk.eagle:library:8004261" deviceset="DIP-CERAMIC-DISC-10NF-50V-Y5V(D4.0MM)" device="" package3d_urn="urn:adsk.eagle:package:8004281/1" value="0.1uf"/>
<part name="U$3" library="LOLIN32" deviceset="DS3231BREAKOUT" device=""/>
<part name="U$4" library="LOLIN32" deviceset="MAX9814BREAKOUT" device=""/>
<part name="5V" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="2,54/1,0" device="" package3d_urn="urn:adsk.eagle:package:30828/1" value="5V"/>
<part name="GND" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="2,54/1,0" device="" package3d_urn="urn:adsk.eagle:package:30828/1" value="GND"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="5.08" y="27.94" smashed="yes">
<attribute name="NAME" x="12.7" y="91.44" size="1.778" layer="95"/>
<attribute name="VALUE" x="12.7" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="SHIFT_REG_DATA" gate="A" x="120.65" y="30.48" smashed="yes">
<attribute name="NAME" x="107.67011875" y="44.6801" size="2.082990625" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="115.339309375" y="7.4331" size="2.08361875" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="U$2" gate="G$1" x="190.5" y="58.42" smashed="yes"/>
<instance part="C2" gate="G$1" x="111.76" y="0" smashed="yes">
<attribute name="NAME" x="105.41" y="3.81" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="113.03" y="3.81" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C4" gate="G$1" x="127" y="0" smashed="yes">
<attribute name="NAME" x="123.19" y="1.27" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="123.19" y="-2.54" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="U$3" gate="G$1" x="106.68" y="83.82" smashed="yes"/>
<instance part="U$4" gate="G$1" x="104.14" y="58.42" smashed="yes"/>
<instance part="5V" gate="P" x="180.34" y="20.32" smashed="yes">
<attribute name="NAME" x="179.197" y="22.1742" size="1.778" layer="95"/>
<attribute name="VALUE" x="179.197" y="17.018" size="1.778" layer="96"/>
</instance>
<instance part="GND" gate="P" x="195.58" y="20.32" smashed="yes">
<attribute name="NAME" x="194.437" y="22.1742" size="1.778" layer="95"/>
<attribute name="VALUE" x="194.437" y="17.018" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="5V" class="1">
<segment>
<pinref part="U$1" gate="G$1" pin="5V"/>
<wire x1="5.08" y1="86.36" x2="-1.27" y2="86.36" width="0.1524" layer="91"/>
<label x="-1.27" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="5V" gate="P" pin="P"/>
<wire x1="182.88" y1="20.32" x2="185.42" y2="20.32" width="0.1524" layer="91"/>
<wire x1="185.42" y1="20.32" x2="185.42" y2="15.24" width="0.1524" layer="91"/>
<label x="182.88" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="1">
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="GND"/>
<wire x1="102.87" y1="17.78" x2="99.06" y2="17.78" width="0.1524" layer="91"/>
<label x="93.98" y="16.51" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="-"/>
<wire x1="115.57" y1="0" x2="115.57" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="115.57" y1="-5.08" x2="114.3" y2="-5.08" width="0.1524" layer="91"/>
<label x="111.76" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="130.81" y1="0" x2="130.81" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="130.81" y1="-5.08" x2="129.54" y2="-5.08" width="0.1524" layer="91"/>
<label x="127" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="GND"/>
<wire x1="111.76" y1="86.36" x2="91.44" y2="86.36" width="0.1524" layer="91"/>
<label x="93.98" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="5.08" y1="30.48" x2="-2.54" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="30.48" x2="-2.54" y2="30.48" width="0.1524" layer="91"/>
<label x="-5.08" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="GND"/>
<wire x1="111.76" y1="60.96" x2="101.6" y2="60.96" width="0.1524" layer="91"/>
<wire x1="99.06" y1="60.96" x2="101.6" y2="60.96" width="0.1524" layer="91"/>
<label x="93.98" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GND" gate="P" pin="P"/>
<wire x1="198.12" y1="20.32" x2="200.66" y2="20.32" width="0.1524" layer="91"/>
<wire x1="200.66" y1="20.32" x2="200.66" y2="15.24" width="0.1524" layer="91"/>
<label x="195.58" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="3V3" class="1">
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="VCC"/>
<wire x1="102.87" y1="38.1" x2="93.98" y2="38.1" width="0.1524" layer="91"/>
<label x="93.98" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="+"/>
<wire x1="107.95" y1="0" x2="107.95" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="107.95" y1="-5.08" x2="106.68" y2="-5.08" width="0.1524" layer="91"/>
<label x="101.6" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="123.19" y1="0" x2="123.19" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="123.19" y1="-5.08" x2="121.92" y2="-5.08" width="0.1524" layer="91"/>
<label x="119.38" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="VCC"/>
<wire x1="111.76" y1="93.98" x2="91.44" y2="93.98" width="0.1524" layer="91"/>
<label x="93.98" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="3.3V"/>
<wire x1="55.88" y1="86.36" x2="63.5" y2="86.36" width="0.1524" layer="91"/>
<label x="60.96" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="VDD"/>
<wire x1="111.76" y1="71.12" x2="99.06" y2="71.12" width="0.1524" layer="91"/>
<label x="93.98" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="SCL"/>
<wire x1="111.76" y1="91.44" x2="91.44" y2="91.44" width="0.1524" layer="91"/>
<label x="93.98" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SDA/GPIO21"/>
<wire x1="5.08" y1="60.96" x2="-7.62" y2="60.96" width="0.1524" layer="91"/>
<label x="-10.16" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="SDA"/>
<wire x1="111.76" y1="88.9" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
<label x="93.98" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SCL/GPIO22"/>
<wire x1="5.08" y1="63.5" x2="-7.62" y2="63.5" width="0.1524" layer="91"/>
<label x="-10.16" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="ADDR0"/>
<wire x1="172.72" y1="68.58" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
<label x="160.02" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="MISO/GPIO19"/>
<wire x1="5.08" y1="68.58" x2="-10.16" y2="68.58" width="0.1524" layer="91"/>
<label x="-10.16" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="ADDR1"/>
<wire x1="172.72" y1="66.04" x2="162.56" y2="66.04" width="0.1524" layer="91"/>
<label x="160.02" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO4"/>
<wire x1="55.88" y1="71.12" x2="73.66" y2="71.12" width="0.1524" layer="91"/>
<label x="73.66" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="ADDR2"/>
<wire x1="172.72" y1="63.5" x2="162.56" y2="63.5" width="0.1524" layer="91"/>
<label x="160.02" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO13/A14"/>
<wire x1="55.88" y1="66.04" x2="76.2" y2="66.04" width="0.1524" layer="91"/>
<label x="73.66" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="ADDR3"/>
<wire x1="172.72" y1="60.96" x2="162.56" y2="60.96" width="0.1524" layer="91"/>
<label x="160.02" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO32/A4"/>
<wire x1="55.88" y1="45.72" x2="73.66" y2="45.72" width="0.1524" layer="91"/>
<label x="73.66" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="ADDR4"/>
<wire x1="172.72" y1="58.42" x2="162.56" y2="58.42" width="0.1524" layer="91"/>
<label x="160.02" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO14/A16"/>
<wire x1="55.88" y1="63.5" x2="73.66" y2="63.5" width="0.1524" layer="91"/>
<label x="73.66" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="ADDR5"/>
<wire x1="172.72" y1="55.88" x2="162.56" y2="55.88" width="0.1524" layer="91"/>
<label x="160.02" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO33/A5"/>
<wire x1="55.88" y1="43.18" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
<label x="73.66" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="CD" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="C/D"/>
<wire x1="172.72" y1="83.82" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<label x="160.02" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SS/LED/GPIO5"/>
<wire x1="5.08" y1="76.2" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<label x="-10.16" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="WR" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="WR"/>
<wire x1="172.72" y1="81.28" x2="167.64" y2="81.28" width="0.1524" layer="91"/>
<label x="160.02" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SCK/GPIO18"/>
<wire x1="5.08" y1="73.66" x2="-10.16" y2="73.66" width="0.1524" layer="91"/>
<label x="-10.16" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="RST"/>
<wire x1="172.72" y1="78.74" x2="167.64" y2="78.74" width="0.1524" layer="91"/>
<label x="160.02" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO15"/>
<wire x1="55.88" y1="60.96" x2="73.66" y2="60.96" width="0.1524" layer="91"/>
<label x="73.66" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="EN" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="EN"/>
<wire x1="172.72" y1="76.2" x2="167.64" y2="76.2" width="0.1524" layer="91"/>
<label x="160.02" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="MOSI/GPIO23"/>
<wire x1="5.08" y1="71.12" x2="-10.16" y2="71.12" width="0.1524" layer="91"/>
<label x="-15.24" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="CLK"/>
<wire x1="102.87" y1="25.4" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<label x="93.98" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO27/A17"/>
<wire x1="55.88" y1="48.26" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
<label x="73.66" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="CLR" class="0">
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="~CLR"/>
<wire x1="102.87" y1="22.86" x2="93.98" y2="22.86" width="0.1524" layer="91"/>
<label x="93.98" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO26/DAC2/A19"/>
<wire x1="55.88" y1="50.8" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
<label x="73.66" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="SHIFTDATA" class="0">
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="B"/>
<pinref part="SHIFT_REG_DATA" gate="A" pin="A"/>
<wire x1="102.87" y1="33.02" x2="102.87" y2="30.48" width="0.1524" layer="91"/>
<wire x1="102.87" y1="30.48" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
<junction x="102.87" y="30.48"/>
<label x="86.36" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO25/DAC1/A18"/>
<wire x1="55.88" y1="53.34" x2="73.66" y2="53.34" width="0.1524" layer="91"/>
<label x="73.66" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="D0" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DATA0"/>
<wire x1="172.72" y1="50.8" x2="160.02" y2="50.8" width="0.1524" layer="91"/>
<label x="160.02" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="QD"/>
<wire x1="138.43" y1="30.48" x2="142.24" y2="30.48" width="0.1524" layer="91"/>
<label x="142.24" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DATA1"/>
<wire x1="172.72" y1="48.26" x2="160.02" y2="48.26" width="0.1524" layer="91"/>
<label x="160.02" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="QH"/>
<wire x1="138.43" y1="20.32" x2="142.24" y2="20.32" width="0.1524" layer="91"/>
<label x="142.24" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DATA2"/>
<wire x1="172.72" y1="45.72" x2="160.02" y2="45.72" width="0.1524" layer="91"/>
<label x="160.02" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="QC"/>
<wire x1="138.43" y1="33.02" x2="142.24" y2="33.02" width="0.1524" layer="91"/>
<label x="142.24" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DATA3"/>
<wire x1="172.72" y1="43.18" x2="160.02" y2="43.18" width="0.1524" layer="91"/>
<label x="160.02" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="QG"/>
<wire x1="138.43" y1="22.86" x2="142.24" y2="22.86" width="0.1524" layer="91"/>
<label x="142.24" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DATA4"/>
<wire x1="172.72" y1="40.64" x2="160.02" y2="40.64" width="0.1524" layer="91"/>
<label x="160.02" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="QB"/>
<wire x1="138.43" y1="35.56" x2="144.78" y2="35.56" width="0.1524" layer="91"/>
<label x="142.24" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DATA5"/>
<wire x1="172.72" y1="38.1" x2="160.02" y2="38.1" width="0.1524" layer="91"/>
<label x="160.02" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="QF"/>
<wire x1="138.43" y1="25.4" x2="142.24" y2="25.4" width="0.1524" layer="91"/>
<label x="142.24" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DATA6"/>
<wire x1="172.72" y1="35.56" x2="160.02" y2="35.56" width="0.1524" layer="91"/>
<label x="160.02" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="QA"/>
<wire x1="138.43" y1="38.1" x2="142.24" y2="38.1" width="0.1524" layer="91"/>
<label x="142.24" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DATA7"/>
<wire x1="172.72" y1="33.02" x2="160.02" y2="33.02" width="0.1524" layer="91"/>
<label x="160.02" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SHIFT_REG_DATA" gate="A" pin="QE"/>
<wire x1="138.43" y1="27.94" x2="142.24" y2="27.94" width="0.1524" layer="91"/>
<label x="142.24" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="MICOUT" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="OUT"/>
<wire x1="111.76" y1="68.58" x2="99.06" y2="68.58" width="0.1524" layer="91"/>
<label x="88.9" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO16"/>
<wire x1="55.88" y1="58.42" x2="73.66" y2="58.42" width="0.1524" layer="91"/>
<label x="73.66" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="MICGAIN" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="GAIN"/>
<wire x1="111.76" y1="66.04" x2="99.06" y2="66.04" width="0.1524" layer="91"/>
<label x="88.9" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GPIO17"/>
<wire x1="55.88" y1="55.88" x2="73.66" y2="55.88" width="0.1524" layer="91"/>
<label x="73.66" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="202,1,5.08,53.34,U$1,RX,,,,"/>
<approved hash="104,1,102.87,38.1,SHIFT_REG_DATA,VCC,3V3,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
