#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <Preferences.h>

typedef enum {
	FormatH12 = 12,
	FormatH24 = 24
} TimeFormat;

class UserSettings {

public:

	UserSettings();
	void init(Preferences * _prefs);

	uint8_t getBrightness();
	uint32_t getColorInterval();
	TimeFormat getTimeFormat();

	// Returns true if successful, false if
	// arguments are invalid.
	bool setBrightness(const uint8_t b);
	bool setColorInterval(const uint32_t seconds);
	bool setTimeFormat(const TimeFormat tf);

protected:

	// Defaults
	Preferences * prefs = NULL;
	TimeFormat timeFormat = FormatH12;
	uint8_t brightness = 4;
	uint32_t colorInterval = 10;

};
