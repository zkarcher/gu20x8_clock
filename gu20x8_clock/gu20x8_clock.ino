#include <stdbool.h>
#include <stdint.h>
#include <Preferences.h>

#include "Animation_IPAddress.h"
#include "Animation_WiFi.h"
#include "ClockDrawer.h"
#include "Clocky.h"
#include "Const.h"
#include "Font_3x5.h"
#include "HttpServer.h"
#include "MathUtil.h"
#include "UserSettings.h"
#include "VFD_Driver.h"

ClockDrawer clockDrawer;
Clocky clocky;
Preferences preferences;
UserSettings settings;
VFD_Driver vfdDriver;
Animation_WiFi animationWifi;
Animation_IPAddress animationIPAddress;

void setup() {
	Serial.begin(115200);
	Serial.println("Hello world!");

	preferences.begin("gu20x8");
	settings.init(&preferences);
	vfdDriver.setBrightness(settings.getBrightness());

	// WiFi sequence
	animationWifi.start(&vfdDriver);
	clocky.initNetworkTime();

	http_server_setup(&clocky, &vfdDriver, &settings);

	animationWifi.stop();

	animationIPAddress.run(&vfdDriver);

	// Wake up the clock
	randomize();
	clockDrawer.init(&clocky, &settings);
}

void loop()
{
	http_server_handle();

	bool didDraw = clockDrawer.update();

	if (didDraw) {
		uint8_t * colorBuf = clockDrawer.getColorBuffer();
		vfdDriver.draw(colorBuf);
	}
}
