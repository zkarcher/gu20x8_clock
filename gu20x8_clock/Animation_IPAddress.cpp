#include <stdbool.h>
#include <stdint.h>
#include <Arduino.h>
#include "Animation_IPAddress.h"
#include "Const.h"
#include "Font_Nx7.h"
#include "VFD_Driver.h"

#ifdef ESP8266
#include <ESP8266WebServer.h>
#else
#include <WebServer.h>
#endif

const int32_t REPEATS = 3;
const int32_t FPS = 12;
const int32_t FRAME_DURATION_MILLIS = 1000 / FPS;

Animation_IPAddress::Animation_IPAddress()
{
}

void Animation_IPAddress::run(VFD_Driver * vfd)
{
	drawIPAddress();

	for (int32_t r = 0; r < REPEATS; r++) {

		// Scroll the IP address
		step = -WIDTH;
		while (step < fboWidth) {
			uint8_t buf[WIDTH * HEIGHT] = {0};

			// Copy each column of the windowed region
			for (int32_t x = 0; x < WIDTH; x++) {
				// Only read within fboWidth
				int32_t offset = step + x;
				if ((offset < 0) || (fboWidth <= offset)) {
					continue;
				}

				uint8_t data = fbo[offset];
				for (uint8_t j = 0; j < HEIGHT; j++) {
					uint8_t mask = 0x1 << j;
					if (data & mask) {
						buf[j * WIDTH + x] = BLUE;
					}
				}
			}

			vfd->draw(buf);

			delay(FRAME_DURATION_MILLIS);
			step++;
		}
	}
}

//
//  PROTECTED
//

void Animation_IPAddress::drawIPAddress()
{
	// Draw the IP address
	for (uint8_t i = 0; i < 4; i++) {
		// Dot '.' separator
		if (i > 0) {
			addBlankColumn();
			drawChar('.');
			addBlankColumn();
		}

		uint8_t addr = WiFi.localIP()[i];

		if (addr >= 100) {
			uint8_t hundreds = addr / 100;
			drawChar('0' + hundreds);
		}

		if (addr >= 10) {
			uint8_t tens = (addr / 10) % 10;
			drawChar('0' + tens);
		}

		uint8_t ones = addr % 10;
		drawChar('0' + ones);
	}
}

void Animation_IPAddress::drawChar(const uint8_t c)
{
	const uint8_t * fontData = getFontCharData(FONT_NX7, c);
	if (fontData == NULL) {
		return;
	}

	while ((fboWidth < MAX_FBO_WIDTH) && ((*fontData) != 0)) {
		fbo[fboWidth++] = *fontData++;
	}

	// Pad right-side with 1px kerning
	addBlankColumn();
}

void Animation_IPAddress::addBlankColumn()
{
	if (fboWidth < MAX_FBO_WIDTH) {
		fboWidth++;
	}
}
