#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <Arduino.h>
#include <WiFiClient.h>

#ifdef ESP8266
#include <ESP8266WebServer.h>
#else
#include <WebServer.h>
#endif

#include "HttpServer.h"
#include "Clocky.h"
#include "UserSettings.h"
#include "VFD_Driver.h"

static WiFiClient client;
static Clocky * _clocky;
static VFD_Driver * _vfdDriver;
static UserSettings * _settings;

#ifdef ESP8266
static ESP8266WebServer server(80);
#else
static WebServer server(80);
#endif

const uint8_t MESSAGE_LEN = 100;
char clientMsg[MESSAGE_LEN + 1];

// most web requests are handled from the root page
// requests look like:
//  /set_brightness?brightness=4

// Forward declarations
static void handle_root(),
	handle_set_brightness(),
	handle_set_color_interval(),
	handle_set_time_format(),
	handle_restart();

void http_server_setup(Clocky * clocky, VFD_Driver * vfdDriver, UserSettings * settings) {
	_clocky = clocky;
	_vfdDriver = vfdDriver;
	_settings = settings;

	server.on("/", handle_root);
	server.on("/set_brightness", handle_set_brightness);
	server.on("/set_color_interval", handle_set_color_interval);
	server.on("/set_time_format", handle_set_time_format);

	server.on("/restart", handle_restart);

	server.onNotFound([]() {
		server.send(404, "text/plain", "File not found");
	});

	server.begin();
}

void http_server_handle() {
	server.handleClient();
}

static void handle_root() {
	byte mac_address[6];
	WiFi.macAddress(mac_address);

	String page = "<html><head>"
	"<meta charset='utf-8'>"
	"<meta name='viewport' content='width=device-width'>"
	"<title>gu20x8 Controls</title>"
	"<style>"
	"body {"
	"    background-color: #330000;"
	"    font-family: sans-serif;"
	"}"
	"h1, h2, label, p, a {"
	"    color: #ffb4b4;"
	"}"
	"p.message {"
	"    color: rgb(255, 200, 100);"
	"    background-color: rgba(255, 200, 100, 0.25);"
	"    padding: 8pt 14pt;"
	"}"
	"</style>"
	"</head>"

	"<body>"
	"<h1>"
	"gu20x8 Controls :)"
	" <span style='color: #000'>&#x2588</span>"
	" <span style='color: #f00'>&#x2588</span>"
	" <span style='color: #0ff'>&#x2588</span>"
	" <span style='color: #fff'>&#x2588</span>"
	"</h1>";

	if (clientMsg[0] != '\0') {
		page += "<p class='message'>";
		page += String(clientMsg);
		page += "</p>";

		// Clear the client message now.
		clientMsg[0] = '\0';
	}

	page +=

	//
	// -- BRIGHTNESS --
	//

	"<hr/><form action='/set_brightness'>"

	// Wake time selectors
	"<h2>Brightness</h2>"

	"<select name='brightness' id='brightness'>";
	for (uint8_t i = 1; i <= 4; i++) {
		page += "<option value='";
		page += String(i);
		if (i == _settings->getBrightness()) {
			page += "' selected>";
		} else {
			page += "'>";
		}
		page += String(i);
		page += "</option>";
	}
	page += "</select><br/>"
	"<br/>"
	"<input type='submit' value='Set brightness'></input>"
	"</form>"

	//
	// -- COLOR INTERVAL --
	//

	"<hr/><form action='/set_color_interval'>"

	// Wake time selectors
	"<h2>Color Interval</h2>"

	"<select name='colorInterval' id='colorInterval'>";

	const int32_t DURATIONS[] = {10, 30, 60, 5*60, 10*60, 15*60, 30*60, 60*60, 12*60*60, 24*60*60};
	const int32_t DURATIONS_LEN = sizeof(DURATIONS) / sizeof(*DURATIONS);

	for (int32_t i = 0; i < DURATIONS_LEN; i++) {
		int32_t dur = DURATIONS[i];
		page += "<option value='";
		page += String(dur);
		if (dur == _settings->getColorInterval()) {
			page += "' selected>";
		} else {
			page += "'>";
		}

		if (dur < 60) {
			page += String(dur) + "s";
		} else if (dur < (60 * 60)) {
			page += String(dur / 60) + "m";
		} else {
			page += String(dur / (60 * 60)) + "h";
		}

		page += "</option>";
	}
	page += "</select><br/>"
	"<br/>"
	"<input type='submit' value='Set color interval'></input>"
	"</form>"

	//
	// -- TIME_FORMAT --
	//

	"<hr/><form action='/set_time_format'>"

	// Wake time selectors
	"<h2>Time Format</h2>"

	"<select name='timeFormat' id='timeFormat'>";
	for (uint8_t i = 12; i <= 24; i += 12) {
		page += "<option value='";
		page += String(i);
		if (i == (uint8_t)(_settings->getTimeFormat())) {
			page += "' selected>";
		} else {
			page += "'>";
		}
		page += String(i);
		page += "hr</option>";
	}
	page += "</select><br/>"
	"<br/>"
	"<input type='submit' value='Set time format'></input>"
	"</form>"

	//
	// -- RESTART --
	//

	"<hr/><a href='/restart'>Restart</a><hr/>";

	//
	// -- STATUS INFO --
	//

	char timeBuf[50];
	_clocky->printTime(timeBuf);

	//"<b>Hostname:</b> " + String(ssid) +
	page += "<p>"
	"<b>Page generated at:</b> " + String(timeBuf) +
	"<br/><b>IP address:</b> " + String(WiFi.localIP()[0]) + "."  + String(WiFi.localIP()[1]) + "."  + String(WiFi.localIP()[2]) + "."  + String(WiFi.localIP()[3]) +

	"<br/><b>MAC address:</b> " + String(mac_address[0], 16) + ":" + String(mac_address[1], 16) + ":" + String(mac_address[2], 16) + ":" + String(mac_address[3], 16) + ":" + String(mac_address[4], 16) + ":" + String(mac_address[5], 16) +

	"<br/><b>Wifi network:</b> " + WiFi.SSID() +
	"<br/><b>RSSI</b>: " + String(WiFi.RSSI()) +
	"<br/><b>subnet mask:</b> " + String(WiFi.subnetMask()[0]) + "."  + String(WiFi.subnetMask()[1]) + "."  + String(WiFi.subnetMask()[2]) + "."  + String(WiFi.subnetMask()[3]) +
	"<br/><b>default router:</b> " + String(WiFi.gatewayIP()[0]) + "."  + String(WiFi.gatewayIP()[1]) + "."  + String(WiFi.gatewayIP()[2]) + "."  + String(WiFi.gatewayIP()[3]) +
	"</p>"
	"</body>"
	"</html>";

	server.send(200, "text/html", page);
}

static void show_message_and_redirect_to_root()
{
	Serial.println(clientMsg);

	server.sendHeader("Location", "/");
	server.send(302);
}

static void handle_set_brightness() {
	bool success = false;

	long int brightness;

	if (server.hasArg("brightness")) {
		brightness = strtol(server.arg("brightness").c_str(), NULL, 10);	// base 10

		success = _settings->setBrightness((const uint8_t)(brightness));
	}

	if (success) {
		sprintf(clientMsg, "Set brightness: %u", brightness);

		_vfdDriver->setBrightness(_settings->getBrightness());

	} else {
		sprintf(clientMsg, "** Invalid brightness");
	}

	show_message_and_redirect_to_root();
}

static void handle_set_color_interval() {
	bool success = false;

	long int interval;

	if (server.hasArg("colorInterval")) {
		interval = strtol(server.arg("colorInterval").c_str(), NULL, 10);	// base 10

		success = _settings->setColorInterval((const uint32_t)(interval));
	}

	if (success) {
		sprintf(clientMsg, "Color interval is set: %u seconds", interval);
	} else {
		sprintf(clientMsg, "** Invalid color interval");
	}

	show_message_and_redirect_to_root();
}

static void handle_set_time_format() {
	bool success = false;

	long int tf;

	if (server.hasArg("timeFormat")) {
		tf = strtol(server.arg("timeFormat").c_str(), NULL, 10);	// base 10

		success = _settings->setTimeFormat((const TimeFormat)(tf));
	}

	if (success) {
		sprintf(clientMsg, "Set time format: %u-hour", tf);
	} else {
		sprintf(clientMsg, "** Invalid time format");
	}

	show_message_and_redirect_to_root();
}

static void handle_restart() {
	server.sendHeader("Location", "/");
	server.send(302);

	delay(500);

	ESP.restart();
}
