#pragma once

#include <stdbool.h>
#include <stdint.h>
#include "Clocky.h"
#include "Const.h"
#include "UserSettings.h"

typedef struct {
	uint8_t bg;
	uint8_t shadow;
	uint8_t stroke;
	uint8_t hilite;
} Palette;

class ClockDrawer {

public:
	ClockDrawer();
	void init (Clocky * _clocky, UserSettings * _settings);
	uint8_t * getColorBuffer();
	bool update();	// Returns true if any graphics changed

protected:
	UserSettings * settings;

	// Pixel data to write
	uint8_t buf[WIDTH * HEIGHT];

	// Crossfade between two times
	uint8_t lastCanvas[WIDTH * HEIGHT];
	uint8_t newCanvas[WIDTH * HEIGHT];

	Clocky * clocky;
	int32_t fadeTimes[WIDTH * HEIGHT];

	Time lastTime;
	Time newTime;
	Palette palettes[2];

	uint32_t lastMillis;
	uint32_t oldMillisSinceFade;
	uint32_t millisSinceFade;

	void canvasDrawTime(uint8_t * canvas, Time * time);
	void canvasDrawChar(uint8_t * canvas, uint8_t charIndex, int8_t x, int8_t y, uint8_t color);
	void canvasSetPixel(uint8_t * canvas, int8_t x, int8_t y, uint8_t color);

	void rerollPalette();
};
