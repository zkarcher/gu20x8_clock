#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include "Const.h"

typedef enum {
	Clocky_OK,
	Clocky_CanRefresh
} ClockyUpdateStatus;

typedef struct Time {
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
};

class Clocky {

public:

	Clocky();
	void initNetworkTime();

	void setRefreshInterval(int32_t interval);

	ClockyUpdateStatus update();

	int32_t getDay();	// Day of month
	int32_t getHour();
	int32_t getMinute();
	int32_t getMonth();
	int32_t getSecond();
	int32_t getWeekday();	// Returns [1-7], 1==Sunday
	int32_t getYear();

	void printTime(char * buf);
	void writeTime(Time * time);

protected:

	time_t baseTime;
	time_t elapsedTime;
	uint32_t lastUpdateMS;

	// Refresh the display every updateInterval seconds
	int32_t refreshInterval;
	time_t lastRefresh;

	time_t getTime();

};
