#include <Arduino.h>
#include <stdint.h>
#include "Const.h"
#include "VFD_Driver.h"

// VFD control
#define CD         (5)
#define WR         (18)	// falling edge latches Address & Data
//#define BUSY     (XX)	// INPUT: 1==busy with last operation
#define RESET      (15)	// Active-low, 0==resets device
#define EN         (23)	// 1==enables display, 0==blank screen

// Data: Shift register interface
#define DATA_OUT   (25)
#define DATA_CLOCK (27)
#define DATA_CLEAR (26)	// Active-low, so keep this high

const uint8_t ADDRESS_PINS[] = {19, 4, 13, 32, 14, 33};

// Shift register pin order: 6,4,2,0, 7,5,3,1
const uint8_t DATA_MASKS[] = {
	0x40, 0x10, 0x04, 0x01,
	0x80, 0x20, 0x08, 0x02
};

VFD_Driver::VFD_Driver()
{
	pinMode(CD, OUTPUT);
	pinMode(WR, OUTPUT);
	//pinMode(BUSY, INPUT);
	pinMode(RESET, OUTPUT);
	pinMode(EN, OUTPUT);

	// Address pins
	for (uint8_t p = 0; p < 6; p++) {
		pinMode(ADDRESS_PINS[p], OUTPUT);
	}

	// Data pins
	pinMode(DATA_OUT, OUTPUT);
	pinMode(DATA_CLOCK, OUTPUT);
	pinMode(DATA_CLEAR, OUTPUT);

	// Start!
	digitalWrite(DATA_CLOCK, LOW);
	digitalWrite(DATA_CLEAR, HIGH);
	digitalWrite(CD, HIGH);
	digitalWrite(WR, HIGH);
	digitalWrite(RESET, HIGH);
	digitalWrite(EN, HIGH);

	// Ready for data
	digitalWrite(CD, HIGH);
	delay(5);
}

void VFD_Driver::draw(uint8_t * colorBuf)
{
	// Send each column to VFD
	for (uint8_t x = 0; x < WIDTH; x++) {

		// Build the data bytes
		uint8_t blue = 0x0;
		uint8_t red = 0x0;
		uint8_t mask = 0x80;

		for (uint8_t b = 0; b < 8; b++) {

			uint8_t color = colorBuf[b * WIDTH + x];
			if (color & BLUE) blue |= mask;
			if (color & RED) red |= mask;

			mask >>= 1;
		}

		const BitOrder BIT_ORDER = k_reversed;

		// Write RED
		digitalWrite(WR, HIGH);	// unlatch
		writeAddress(0x20 | x);	// red address
		writeDataByte(red, BIT_ORDER);
		digitalWrite(WR, LOW);	// latch

		// Delay after falling WR is important.
		// Without this, bad/wrong pixels can appear. Why?
		// On my board, I'm not using BUSY line, it does
		// not seem to indicate anything useful.
		delayMicroseconds(1);

		// Write BLUE
		digitalWrite(WR, HIGH);	// unlatch
		writeAddress(0x0 | x);	// blue address
		writeDataByte(blue, BIT_ORDER);
		digitalWrite(WR, LOW);	// latch
		delayMicroseconds(1);
	}
}

// Expects range: 1..4
void VFD_Driver::setBrightness(uint8_t brightness)
{
	if ((brightness < 1) || (4 < brightness)) return;

	writeAddress(0x3f);
	writeDataByte(brightness - 1, k_normal);

	// Unlatch
	digitalWrite(WR, HIGH);
	digitalWrite(CD, LOW);

	// Wait until not busy, then latch.
	//while (digitalRead(BUSY) == HIGH) {}
	delay(1);

	// OK to latch now.
	digitalWrite(WR, LOW);
	delay(1);

	// Restore normal operation
	digitalWrite(CD, HIGH);
}

// -- PROTECTED --

void VFD_Driver::writeAddress(uint8_t value)
{
	for (uint8_t i = 0; i < 6; i++) {
		uint8_t mask = 0x20 >> i;
		digitalWrite(ADDRESS_PINS[i], (value & mask) ? HIGH : LOW);
	}
}

void VFD_Driver::writeDataByte(uint8_t value, BitOrder bitOrder)
{
	for (int8_t i = 7; i >= 0; i--) {
		digitalWrite(DATA_CLOCK, LOW);

		uint8_t maskIdx = (bitOrder == k_reversed) ? (7 - i) : i;
		uint8_t mask = DATA_MASKS[maskIdx];
		digitalWrite(DATA_OUT, (value & mask) ? HIGH : LOW);

		// Clock 'em
		digitalWrite(DATA_CLOCK, HIGH);
	}
}
