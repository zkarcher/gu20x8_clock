#include <Arduino.h>
#include <stdbool.h>
#include <stdint.h>
#include "Animation_WiFi.h"
#include "Const.h"
#include "VFD_Driver.h"

const uint32_t FPS = 3;
const uint32_t FRAME_DURATION = 1000000 / FPS;

volatile uint32_t step = 0;
VFD_Driver * _vfd = NULL;

// One mirror half of WiFi image
const int8_t wifiImage[] = {
	0,0,0,0,0,0,4,4,4,4,
	0,0,0,0,0,4,0,0,0,0,
	0,0,0,0,4,0,0,3,3,3,
	0,0,0,4,0,0,3,0,0,0,
	0,0,0,0,0,3,0,0,2,2,
	0,0,0,0,0,0,0,2,0,0,
	0,0,0,0,0,0,0,0,0,1,
	0,0,0,0,0,0,0,0,0,1,
};

void IRAM_ATTR perFrame()
{
	step++;

	int8_t phase = (step & 0x7) + 1;

	const int8_t * img = &wifiImage[0];

	// Set pixel colors here:
	uint8_t colorBuf[WIDTH * HEIGHT] = {0};

	for (uint8_t j = 0; j < HEIGHT; j++) {
		for (uint8_t i = 0; i < WIDTH / 2; i++) {
			int8_t px = (*img);

			// Pixel is set?
			if (px) {
				// Emitting waves
				uint8_t clr = ((px < phase) && (px >= (phase - 4))) ? WHITE : BLUE;

				// wifiImage is one half, draw on both sides
				colorBuf[j * WIDTH + i] = clr;
				colorBuf[j * WIDTH + ((WIDTH - 1) - i)] = clr;
			}

			img++;
		}
	}

	_vfd->draw(colorBuf);
}

Animation_WiFi::Animation_WiFi()
{
	timer = NULL;
}

void Animation_WiFi::start(VFD_Driver * vfd)
{
	step = 0;
	_vfd = vfd;

	const uint8_t TIMER_ID = 0;
	const uint32_t CLOCK_PRESCALE = 80;
	const bool RISING_EDGE = true;
	timer = timerBegin(TIMER_ID, CLOCK_PRESCALE, RISING_EDGE);
	timerAttachInterrupt(timer, perFrame, true);
	timerAlarmWrite(timer, FRAME_DURATION, true);
	timerAlarmEnable(timer);
}

void Animation_WiFi::stop()
{
	if (timer) {
		timerAlarmDisable(timer);
		timerDetachInterrupt(timer);
		timerEnd(timer);
	}

	timer = NULL;
}
