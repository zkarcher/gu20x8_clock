#pragma once

#include <stdbool.h>

#define WIDTH          (20)
#define HEIGHT         (8)

// Convenient color flags
#define BLACK          (0)
#define BLUE           (1)
#define RED            (2)
#define WHITE          (BLUE | RED)

#define LAYER_COUNT    (5)
