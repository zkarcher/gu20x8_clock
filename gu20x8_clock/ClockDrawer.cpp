#include <Arduino.h>
#include <stdint.h>
#include "ClockDrawer.h"
#include "Const.h"
#include "Font_3x5.h"
#include "MathUtil.h"
#include "UserSettings.h"

// -- CANVAS DRAWING --
#define BG       (1)
#define STROKE   (2)
#define SHADOW   (4)
#define HILITE   (8)	// PM indicator

ClockDrawer::ClockDrawer()
{
}

void ClockDrawer::init(Clocky * _clocky, UserSettings * _settings) {
	clocky = _clocky;
	settings = _settings;

	clocky->writeTime(&newTime);
	lastTime = newTime;
	//lastTime.second = 0xff;	// force a redraw

	/*
	for (uint8_t i = 0; i < WIDTH * HEIGHT; i++) {
		fadeTimes[i] = 0;
	}
	*/

	// millisSinceFade: Governs the fade animation
	millisSinceFade = 0;	// Restart anim
	oldMillisSinceFade = 0;
	lastMillis = millis();

	// Choose a palette, force a transition
	rerollPalette();

	// Hack: Fade in from black
	palettes[0] = (Palette){
		.bg = BLACK,
		.shadow = BLACK,
		.stroke = BLACK,
		.hilite = BLACK
	};
	palettes[1] = (Palette){
		.bg = RED,
		.shadow = BLACK,
		.stroke = WHITE,
		.hilite = BLUE
	};

	// Ensure time is drawn in buffers before displaying.
	canvasDrawTime(lastCanvas, &lastTime);
	canvasDrawTime(newCanvas, &newTime);
}

uint8_t * ClockDrawer::getColorBuffer()
{
	return buf;
}

// Returns true if any graphics changed
bool ClockDrawer::update()
{
	bool didDraw = false;

	int32_t oldMillisSinceFade = millisSinceFade;

	uint32_t now = millis();
	uint32_t elapsed = now - lastMillis;
	millisSinceFade += elapsed;
	lastMillis = now;

	clocky->update();
	Time thisTime;
	clocky->writeTime(&thisTime);

	bool isColorChange = false;
	bool isMinuteChange = thisTime.minute != newTime.minute;
	bool isSecondChange = thisTime.second != newTime.second;

	uint32_t interval = settings->getColorInterval();
	uint32_t fromTime = newTime.hour * 60 * 60 + newTime.minute * 60 + newTime.second;
	uint32_t toTime = thisTime.hour * 60 * 60 + thisTime.minute * 60 + thisTime.second;

	if (interval < 24 * 60 * 60) {
		fromTime /= interval;
		toTime /= interval;
		isColorChange = (fromTime != toTime);

	} else {
		// 24 hours
		isColorChange = toTime < fromTime;
	}

	// Minute changed? Reroll the palette, advance the time
	if (isColorChange) {
		rerollPalette();

		// Update the times: Crossfade from newTime -> thisTime
		lastTime = newTime;
		newTime = thisTime;

		// millisSinceFade: Governs the fade animation
		millisSinceFade = 0;	// Restart anim
	}

	// Long colorIntervals: Please copy newTime to lastTime,
	// so the correct time will be drawn.
	if (!isColorChange && isMinuteChange && (interval > 60)) {
		newTime = thisTime;
	}

	// Seconds changed? Redraw.
	if (isSecondChange) {
		// Hack to ensure colon blinks:
		lastTime.second = thisTime.second;
		newTime.second = thisTime.second;

		canvasDrawTime(lastCanvas, &lastTime);
		canvasDrawTime(newCanvas, &newTime);

		didDraw = true;

		// Print!
		char strbuf[32];
		clocky->printTime(strbuf);
		Serial.println(strbuf);
	}

	// Any pixels changing state?
	int32_t * ft = &fadeTimes[0];
	for (uint8_t i = 0; i < WIDTH * HEIGHT; i++) {
		if (oldMillisSinceFade <= (*ft) && (*ft) < millisSinceFade) {
			didDraw = true;
			break;
		}
		ft++;
	}

	// Any changes? Update color buffer.
	if (didDraw) {
		for (uint8_t i = 0; i < WIDTH * HEIGHT; i++) {
			bool isNew = (fadeTimes[i] < millisSinceFade);
			uint8_t * canvas = (uint8_t *)(isNew ? newCanvas : lastCanvas);
			Palette * pal = isNew ? (&palettes[1]) : (&palettes[0]);

			if (canvas[i] & BG) {
				buf[i] = pal->bg;
			} else if (canvas[i] & STROKE) {
				buf[i] = pal->stroke;
			} else if (canvas[i] & SHADOW) {
				buf[i] = pal->shadow;
			} else {
				buf[i] = pal->hilite;
			}
		}
	}

	oldMillisSinceFade = millisSinceFade;

	return didDraw;
}

// -- PROTECTED --

void ClockDrawer::canvasDrawTime(uint8_t * canvas, Time * time)
{
	TimeFormat timeFormat = settings->getTimeFormat();
	bool h12 = (timeFormat == FormatH12);

	// Clear the canvas
	for (uint8_t i = 0; i < WIDTH * HEIGHT; i++) {
		canvas[i] = BG;
	}

	uint8_t hour = time->hour;
	uint8_t minute = time->minute;
	uint8_t second = time->second;

	// 12-hour format
	bool isPM = false;
	if (h12) {
		isPM = (hour >= 12);
		hour %= 12;
		if (!hour) hour = 12;	// 0 o'clock => 12
	}

	// Draw shadows, then solid numerals
	for (int8_t s = 1; s >= 0; s--) {	// 1==shadow, 0==stroke
		uint8_t color = (s == 1) ? SHADOW : STROKE;

		// 24hour: Always shows 4 digits, 00:00
		int8_t xOffset = 0;
		if (h12) {
			// 12hour: Scooch left, so it's more centered
			xOffset = -2;
			if (!isPM && (hour >= 10)) {
				xOffset += 1;
			}
		}

		// Draw hour
		if (!h12 || (hour >= 10)) {
			canvasDrawChar(canvas, hour / 10, xOffset + 1 + s, 1 + s, color);
		}
		canvasDrawChar(canvas, hour % 10, xOffset + 5 + s, 1 + s, color);

		// Draw blinking colon
		if ((~second) & 0x1) {
			const uint8_t COLON = 10;
			canvasDrawChar(canvas, COLON, xOffset + 9 + s, 1 + s, color);
		}

		// Draw minutes
		canvasDrawChar(canvas, minute / 10, xOffset + 11 + s, 1 + s, color);
		canvasDrawChar(canvas, minute % 10, xOffset + 15 + s, 1 + s, color);

		// Draw PM marker using 4th color (HILITE)
		if (isPM && (s == 0)) {
			const uint8_t PM_MARKER = 11;
			canvasDrawChar(canvas, PM_MARKER, 18 + s, 1 + s, (s == 0) ? HILITE : color);
		}
	}
}

void ClockDrawer::canvasDrawChar(uint8_t * canvas, uint8_t charIndex, int8_t x, int8_t y, uint8_t color)
{
	for (int8_t i = 0; i < 3; i++) {
		uint8_t data = FONT_3X5[charIndex * 3 + i];

		for (int8_t j = 0; j < 5; j++) {
			uint8_t mask = 0x1 << j;
			if (data & mask) {
				canvasSetPixel(canvas, x + i, y + j, color);
			}
		}
	}
}

void ClockDrawer::canvasSetPixel(uint8_t * canvas, int8_t x, int8_t y, uint8_t color)
{
	// Pixel in display area?
	if (x < 0) return;
	if (WIDTH <= x) return;
	if (y < 0) return;
	if (HEIGHT <= y) return;

	canvas[y * WIDTH + x] = color;
}

void ClockDrawer::rerollPalette()
{
	uint8_t pals[] = {
		//rating
		//  BG   TEXT   SHADOW
		0,	BLACK, RED,   BLUE,
		0,	BLACK, RED,   WHITE,
		2,	BLACK, BLUE,  RED,
		2,	BLACK, BLUE,  WHITE,
		2,	BLACK, WHITE, RED,
		2,	BLACK, WHITE, BLUE,

		0,	RED,   BLACK, BLUE,
		2,	RED,   BLACK, WHITE,
		0,	RED,   BLUE,  BLACK,
		0,	RED,   BLUE,  WHITE,
		2,	RED,   WHITE, BLACK,
		0,	RED,   WHITE, BLUE,

		2,	BLUE,  BLACK, RED,
		2,	BLUE,  BLACK, WHITE,
		0,	BLUE,  RED,   BLACK,
		0,	BLUE,  RED,   WHITE,
		2,	BLUE,  WHITE, BLACK,
		0,	BLUE,  WHITE, RED,

		2,	WHITE, BLACK, RED,
		2,	WHITE, BLACK, BLUE,
		0,	WHITE, RED,   BLACK,
		0,	WHITE, RED,   BLUE,
		0,	WHITE, BLUE,  BLACK,
		0,	WHITE, BLUE,  RED,
		// Oddly satisfying to type these by hand.
	};
	uint8_t palsCount = (sizeof(pals) / sizeof(pals[0])) / 4;

	// Try to find a palette where all 3 colors change:
	for (uint8_t i = 0; i < 100; i++) {
		uint8_t pIndex = random(palsCount);

		uint8_t rating = pals[pIndex * 4];
		if (rating == 0) continue;

		uint8_t bg = pals[pIndex * 4 + 1];
		uint8_t stroke = pals[pIndex * 4 + 2];
		uint8_t shadow = pals[pIndex * 4 + 3];

		// bg must change, and either stroke|shadow changes
		if (bg == palettes[1].bg) continue;
		if ((stroke == palettes[1].stroke) && (shadow == palettes[1].shadow)) continue;

		// Hilite color is the missing 4th color
		uint8_t hilite = (BLACK + RED + BLUE + WHITE) - (bg + stroke + shadow);

		palettes[0] = palettes[1];
		palettes[1] = (Palette){
			.bg = bg,
			.shadow = shadow,
			.stroke = stroke,
			.hilite = hilite
		};
		break;
	}

	// Transition times
	uint32_t speed = random(50, 57);	// higher == slower
	uint8_t noise = random(180, 230);	// 220 is good

	// Which direction?
	const uint8_t DIR_OPTIONS = 9;
	uint8_t dir = random(0, DIR_OPTIONS);

	// Diagonal is not as satisfying to me, so... reroll?
	if ((4 <= dir) && (dir <= 7)) {
		dir = random(0, DIR_OPTIONS);
	}

	// Adjustments
	if ((dir == 2) || (dir == 3)) {
		// Vertical
		speed *= randf(1.0f, 1.5f);	// Slower motion

	} else if ((4 <= dir) && (dir <= 7)) {
		// Diagonal
		speed *= 0.707f;	// Compensate for longer movement

	} else if (dir == 8) {
		// Radial
		speed *= randf(1.75f, 2.0f);	// Slower motion
		noise *= randf(0.5f, 1.0f);	// Less noise
	}

	int32_t * ft = &fadeTimes[0];
	for (int8_t y = 0; y < HEIGHT; y++) {
		for (int8_t x = 0; x < WIDTH; x++) {
			uint8_t n = random(noise);

			switch (dir) {
				// horizontal
				case 0: *ft = x * speed + n; break;
				case 1: *ft = ((WIDTH - 1) - x) * speed + n; break;

				// vertical
				case 2: *ft = y * speed + n; break;
				case 3: *ft = ((HEIGHT - 1) - y) * speed + n; break;

				// diagonal
				case 4:
				case 5:
				case 6:
				case 7:
				{
					uint8_t dx = (dir & 0x1) ? x : ((WIDTH - 1) - x);
					uint8_t dy = (dir & 0x2) ? y : ((HEIGHT - 1) - y);
					*ft = (dx + dy) * speed + n;
				}
				break;

				// radial from center
				case 8:
				{
					float dx = x - (WIDTH / 2);
					float dy = y - (HEIGHT / 2);
					float distance = sqrtf(dx * dx + dy * dy);
					*ft = distance * speed + n;
				}
				break;
			}

			ft++;
		}
	}
}
