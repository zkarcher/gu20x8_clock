#pragma once

#include "Clocky.h"
#include "UserSettings.h"
#include "VFD_Driver.h"

void http_server_setup(Clocky * clocky, VFD_Driver * vfdDriver, UserSettings * settings);
void http_server_handle();
