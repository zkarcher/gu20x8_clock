#pragma once

#include <stdint.h>
#include "VFD_Driver.h"

const int32_t MAX_FBO_WIDTH = 12 * 5 + 3 * 2;	 // IPV4

class Animation_IPAddress {

public:
	Animation_IPAddress();
	void run(VFD_Driver * vfd);

protected:
	int32_t step;
	int32_t fboWidth;
	uint8_t fbo[MAX_FBO_WIDTH];

	void drawIPAddress();
	void drawChar(const uint8_t c);
	void addBlankColumn();
};
