#include <Arduino.h>
#include <limits.h>
#include <NTPClient.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <TimeLib.h>
#include <Timezone.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <WiFiUdp.h>
#include "Clocky.h"
#include "WiFi_Credentials.h"

const char * MONTH_ABBRS[] = {
	"Jan", "Feb", "Mar", "Apr", "May", "Jun",
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

#define NTP_INTERVAL  (INT_MAX)   // In miliseconds
#define NTP_ADDRESS   "pool.ntp.org"

// US Pacific Time Zone (Portland Oregon)
TimeChangeRule ptDST = {"PDT", Second, Sun, Mar, 2, -420};  // Daylight time = UTC - 7 hours
TimeChangeRule ptSTD = {"PST", First, Sun, Nov, 2, -480};   // Standard time = UTC - 8 hours
Timezone timeRule(ptDST, ptSTD);

Clocky::Clocky() {
	baseTime = 0;
	elapsedTime = 0;
	lastUpdateMS = millis();
	refreshInterval = 0;
	lastRefresh = 0;
}

void Clocky::initNetworkTime()
{
	WiFiMulti wifiMulti;
	for (uint32_t i = 0; i < CREDENTIALS_LEN; i++) {
		wifiMulti.addAP(CREDENTIALS[i].ssid, CREDENTIALS[i].password);
	}

	Serial.println("Connecting ");
	while (wifiMulti.run() != WL_CONNECTED) {
		Serial.print(".");
		delay(1000);
	}

	delay(1000);
	Serial.println("");
	Serial.print("WiFi connected to SSID:  ");
	Serial.println(WiFi.SSID());
	Serial.print("IP address:              ");
	Serial.println(WiFi.localIP());

	WiFiUDP ntpUDP;
	const long INITIAL_NTP_OFFSET = 0;
	NTPClient timeClient(ntpUDP, NTP_ADDRESS, INITIAL_NTP_OFFSET, NTP_INTERVAL);

	timeClient.begin();
	while (!timeClient.forceUpdate()) {
		// forceUpdate() returns true after update occurs.
	}

	baseTime = timeClient.getEpochTime();
	elapsedTime = 0;
	lastUpdateMS = millis();

	// Disconnect
	timeClient.end();

	int32_t hour = getHour();
	int32_t minute = getMinute();
	int32_t second = getSecond();
	Serial.print("Time: ");
	Serial.print(hour);
	Serial.print(":");
	if (minute < 10) Serial.print("0");
	Serial.print(minute);
	Serial.print(":");
	if (second < 10) Serial.print("0");
	Serial.println(second);
}

void Clocky::setRefreshInterval(int32_t interval)
{
	refreshInterval = interval;
}

ClockyUpdateStatus Clocky::update()
{
	uint32_t nowMS = millis();
	uint32_t elapsedMS = nowMS - lastUpdateMS;

	if (elapsedMS >= 1000) {
		uint32_t elapsedSeconds = elapsedMS / 1000;

		// Advance currentTime
		elapsedTime += elapsedSeconds;

		lastUpdateMS += elapsedSeconds * 1000;
	}

	// Can redraw this update?
	if (refreshInterval > 0) {
		time_t now = getTime();

		time_t refreshFloor = now / refreshInterval;
		refreshFloor *= refreshInterval;

		if ((refreshFloor - lastRefresh) >= refreshInterval) {
			lastRefresh = refreshFloor;
			return Clocky_CanRefresh;
		}
	}

	return Clocky_OK;
}

int32_t Clocky::getDay()
{
	return day(getTime());
}

int32_t Clocky::getHour()
{
	return hour(getTime());
}

int32_t Clocky::getMinute()
{
	return minute(getTime());
}

int32_t Clocky::getMonth()
{
	return month(getTime());
}

int32_t Clocky::getSecond()
{
	return second(getTime());
}

// Returns [1-7], 1==Sunday
int32_t Clocky::getWeekday()
{
	return weekday(getTime());
}

int32_t Clocky::getYear()
{
	return year(getTime());
}

void Clocky::printTime(char * buf)
{
	// FIXME we don't need the whole <string> library
	sprintf(buf, "%02i:%02i:%02i",
		getHour(),
		getMinute(),
		getSecond()
	);
}

void Clocky::writeTime(Time * time)
{
	time->hour = getHour();
	time->minute = getMinute();
	time->second = getSecond();
}

// -- PROTECTED --

time_t Clocky::getTime()
{
	return timeRule.toLocal(baseTime + elapsedTime);
}
