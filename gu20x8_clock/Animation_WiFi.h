#pragma once

#include <stdint.h>
#include "VFD_Driver.h"

class Animation_WiFi {

public:
	Animation_WiFi();
	void start(VFD_Driver * vfd);
	void stop();

protected:
	hw_timer_t * timer;
};
