#pragma once

#include <stdint.h>

// First byte is the char's ASCII value,
// followed by image data, ending in 0.
// This is not optimized :P

const uint8_t FONT_NX7[] = {

	'0',
	0b0111110,
	0b1000001,
	0b1000001,
	0b0111110,
	0,

	'1',
	0b1000010,
	0b1111111,
	0b1000000,
	0,

	'2',
	0b1110010,
	0b1001001,
	0b1001001,
	0b1000110,
	0,

	'3',
	0b0100010,
	0b1001001,
	0b1001001,
	0b0110110,
	0,

	'4',
	0b0001111,
	0b0001000,
	0b0001000,
	0b1111111,
	0,

	'5',
	0b1001111,
	0b1001001,
	0b1001001,
	0b0110001,
	0,

	'6',
	0b0111110,
	0b1001001,
	0b1001001,
	0b0110010,
	0,

	'7',
	0b0000011,
	0b1110001,
	0b0001001,
	0b0000111,
	0,

	'8',
	0b0110110,
	0b1001001,
	0b1001001,
	0b0110110,
	0,

	'9',
	0b1000110,
	0b1001001,
	0b1001001,
	0b0111110,
	0,

	'.',
	0b1000000,
	0,

	0 	// END
};

// Returns NULL if not found
const uint8_t * getFontCharData(const uint8_t * font, const uint8_t ch)
{
	while (true) {
		// Reached the end? (Character not found)
		if ((*font) == 0) {
			return NULL;
		}

		// Found the character?
		if ((*font) == ch) {
			return ++font;	// Step over the ASCII value
		}

		// Step over this char, until we find a 0
		font++;	// Step over the ASCII value
		while (*font) {
			font++;
		}

		// Step over the 0
		font++;
	}

	return NULL;	// Shouldn't reach this point.
}
