#include <stdbool.h>
#include <stdint.h>
#include <Preferences.h>

#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"

#include "UserSettings.h"

const char * BRIGHTNESS_KEY = "brightness";
const char * COLOR_INTERVAL_KEY = "colorInterval";
const char * TIME_FORMAT_KEY = "timeFormat";

UserSettings::UserSettings()
{
}

void UserSettings::init(Preferences * _prefs)
{
	prefs = _prefs;

	// Fall back on defaults
	brightness = prefs->getUChar(BRIGHTNESS_KEY, brightness);
	colorInterval = prefs->getUInt(COLOR_INTERVAL_KEY, colorInterval);
	timeFormat = (TimeFormat)(prefs->getUChar(TIME_FORMAT_KEY, timeFormat));
}

uint8_t UserSettings::getBrightness()
{
	return brightness;
}

uint32_t UserSettings::getColorInterval()
{
	return colorInterval;
}

TimeFormat UserSettings::getTimeFormat()
{
	return timeFormat;
}

bool UserSettings::setBrightness(const uint8_t b)
{
	if ((b < 1) || (4 < b)) return false;

	brightness = b;

	size_t sz = prefs->putUChar(BRIGHTNESS_KEY, b);
	return (sz > 0);
}

bool UserSettings::setColorInterval(const uint32_t interval)
{
	colorInterval = interval;

	size_t sz = prefs->putUInt(COLOR_INTERVAL_KEY, interval);
	return (sz > 0);
}

bool UserSettings::setTimeFormat(const TimeFormat tf)
{
	timeFormat = tf;

	size_t sz = prefs->putUChar(TIME_FORMAT_KEY, (const uint8_t)(timeFormat));
	return (sz > 0);
}
