#pragma once

#include <stdint.h>

typedef enum {
	k_normal = 0,
	k_reversed = 1
} BitOrder;

class VFD_Driver {

public:
	VFD_Driver();
	void draw(uint8_t * colorBuf);
	void setBrightness(uint8_t brightness);	// 1..4

protected:
	void writeAddress(uint8_t value);
	void writeDataByte(uint8_t value, BitOrder bitOrder);
};
